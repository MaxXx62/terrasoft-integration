using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Terrasoft.Core;
using Terrasoft.Core.Entities;


namespace SiblionIntegration
{
    #region Enums
    public enum WSDirection { OUTBOUND, INBOUND }

    public enum OperationState { New, InProgress, Complete, Error };
    #endregion

    #region Interfaces
    public interface ITypeWithAlternativeSerializer
    {
        XmlSerializer GetAlternativeSerializer();
    }
    #endregion

    #region Classes
    public class ServiceInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public WSDirection Direction { get; set; }
        public string DirectionName { get => SibIntegrationConstants.WSDirectionNames[Direction]; }
        public string URL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Timeout { get; set; }
    }

    public class ServiceUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
    }

    public class ServiceResult
    {
        public bool IsSuccess { get; set; }
        public object Result { get; set; }
    }

    public abstract class SimpleHierarchyMember
    {
        private int _positionInHierarchy;

        public int PositionInHierarchy { get => _positionInHierarchy; }
        public abstract string IdField { get; }
        public abstract string ParentIdField { get; }

        [XmlIgnore]
        public SimpleHierarchyMember Parent { get; set; }

        public void SetPositionInHierarchy(IEnumerable<SimpleHierarchyMember> hierarchyCollection)
        {
            if (_positionInHierarchy != 0) return;
            _positionInHierarchy = 1;

            if (String.IsNullOrEmpty(ParentIdField) || IdField == ParentIdField)
            {
                return;
            }

            var parent = hierarchyCollection.FirstOrDefault(x => x.IdField == ParentIdField);

            if (parent == null)
            {
                _positionInHierarchy = -1;
                return;
            }
            Parent = parent;
            if (parent.PositionInHierarchy == 0) parent.SetPositionInHierarchy(hierarchyCollection);
            _positionInHierarchy = parent.PositionInHierarchy == -1 ? 1 : parent.PositionInHierarchy + 1;
        }

        public static void InitHierarchy(IEnumerable<SimpleHierarchyMember> hierarchyCollection)
        {
            foreach (var item in hierarchyCollection)
            {
                item.SetPositionInHierarchy(hierarchyCollection);
            }
        }
    }
    #endregion

    #region Exceptions
    public class WSExternalSystemException : Exception
    {
        public WSExternalSystemException() { }

        public WSExternalSystemException(string message) : base(message) { }

        public WSExternalSystemException(string message, Exception innerException) : base(message, innerException) { }

        protected WSExternalSystemException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Исключение, не выходящее из Executor, при обработке которого в логе интеграции фиксируется только текст ошибки (без простановки статуса = "Ошибка").
    /// </summary>
    public class WSExpectedException : Exception
    {
        public WSExpectedException() { }

        public WSExpectedException(string message) : base(message) { }

        public WSExpectedException(string message, Exception innerException) : base(message, innerException) { }

        protected WSExpectedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Исключение, выходящее из Executor, при обработке которого в логе интеграции фиксируется только текст ошибки (без простановки статуса = "Ошибка").
    /// </summary>
    public class WSBusinessException : Exception
    {
        public WSBusinessException() { }

        public WSBusinessException(string message) : base(message) { }

        public WSBusinessException(string message, Exception innerException) : base(message, innerException) { }

        protected WSBusinessException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class WSOutboundPrepareDataException : Exception
    {
        public WSOutboundPrepareDataException() { }

        public WSOutboundPrepareDataException(string message) : base(message) { }

        public WSOutboundPrepareDataException(string message, Exception innerException) : base(message, innerException) { }

        protected WSOutboundPrepareDataException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class WSOutboundCallWebServiceException : Exception
    {
        public WSOutboundCallWebServiceException() { }

        public WSOutboundCallWebServiceException(string message) : base(message) { }

        public WSOutboundCallWebServiceException(string message, Exception innerException) : base(message, innerException) { }

        protected WSOutboundCallWebServiceException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public class WSOutboundProcessDataException : Exception
    {
        public WSOutboundProcessDataException() { }

        public WSOutboundProcessDataException(string message) : base(message) { }

        public WSOutboundProcessDataException(string message, Exception innerException) : base(message, innerException) { }

        protected WSOutboundProcessDataException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
    #endregion

    #region Extensions
    public static class EntityExtension
    {
        public static bool SetColumnValueWithNull(this Entity entity, string name, object value)
        {
            if (value == null)
            {
                return entity.SetColumnValue(name, value);
            }
            else if (value.GetType() == typeof(Guid) && (Guid)value == Guid.Empty)
            {
                return entity.SetColumnValue(name, null);
            }
            else if (value.GetType() == typeof(string) && String.IsNullOrEmpty(value.ToString()))
            {
                return entity.SetColumnValue(name, null);
            }
            else if (value.GetType() == typeof(DateTime) && (DateTime)value == DateTime.MinValue)
            {
                return entity.SetColumnValue(name, null);
            }
            else
            {
                return entity.SetColumnValue(name, value);
            }
        }

        public static bool SetColumnValueWithFloat(this Entity entity, string name, float value)
        {
            var currentValue = entity.GetTypedColumnValue<float>(name);
            var isEqual = Math.Abs(value - currentValue) <= 0.00001;
            if (!isEqual)
            {
                return entity.SetColumnValue(name, value);
            }
            else return false;
        }

        public static bool SetColumnValueWithDecimal(this Entity entity, string name, decimal value)
        {
            var currentValue = entity.GetTypedColumnValue<decimal>(name);
            var isEqual = Math.Abs(value - currentValue) <= Convert.ToDecimal(0.000001);
            if (!isEqual)
            {
                return entity.SetColumnValue(name, value);
            }
            else return false;
        }

        public static bool SetColumnValueWithDecimal(this Entity entity, string name, float value, int precission = 2)
        {
            var convertedValue = Convert.ToDecimal(Math.Round(value, precission));
            return SetColumnValueWithDecimal(entity, name, convertedValue);
        }

        public static bool SetColumnValueWithDecimal(this Entity entity, string name, string value, int precission = 2)
        {
            if (String.IsNullOrEmpty(value)) value = "0"; 

            var convertedValue = Convert.ToDecimal(Math.Round(Decimal.Parse(value), precission));
            return SetColumnValueWithDecimal(entity, name, convertedValue);
        }

        public static bool SetColumnValueWithBoolean(this Entity entity, string name, object value)
        {
            if (value == null)
            {
                return entity.SetColumnValue(name, false);
            }
            else if (value.GetType() == typeof(string) && String.IsNullOrEmpty((string)value))
            {
                return entity.SetColumnValue(name, false);
            }
            else if (value.GetType() == typeof(string) && !String.IsNullOrEmpty((string)value))
            {
                var isSuccess = Boolean.TryParse(((string)value).ToLower(), out var booleanValue);
                return (isSuccess) ? entity.SetColumnValue(name, booleanValue): false;
            }
            else
            {
                return entity.SetColumnValue(name, value);
            }
        }

        public static bool SetColumnValueWithInteger(this Entity entity, string name, object value)
        {
            if (value == null)
            {
                return entity.SetColumnValue(name, 0);
            }
            else if (value.GetType() == typeof(string) && String.IsNullOrEmpty((string)value))
            {
                return entity.SetColumnValue(name, 0);
            }
            else if (value.GetType() == typeof(string) && !String.IsNullOrEmpty((string)value))
            {
                var isSuccess = Int32.TryParse((string)value, out var intValue);
                return (isSuccess) ? entity.SetColumnValue(name, intValue) : false;
            }
            else if (value.GetType() == typeof(float))
            {
                return entity.SetColumnValue(name, Math.Round((float)value));
            }
            else
            {
                return entity.SetColumnValue(name, value);
            }
        }

        public static bool SetColumnValueIfNotNull(this Entity entity, string name, object value)
        {
            if (value != null)
            {
                return entity.SetColumnValue(name, value);
            }
            else
            {
                return false;
            }
        }
    }
    #endregion

    #region SibIntegrationConstants
    public static class SibIntegrationConstants
    {
        public static Dictionary<WSDirection, string> WSDirectionNames { get; } = new Dictionary<WSDirection, string> {
            [WSDirection.OUTBOUND] = "Исходящая",
            [WSDirection.INBOUND]  = "Входящая"
        };

        public static Dictionary<OperationState, Guid> OperationStates { get; } = new Dictionary<OperationState, Guid>
        {
            [OperationState.New]        = new Guid("E3385F50-38F6-4D15-8331-8036CB7A2CE0"),
            [OperationState.InProgress] = new Guid("4C627D12-8FAB-41D6-B158-8E46D78FE446"),
            [OperationState.Complete]   = new Guid("7F8338B5-C476-4389-9CF8-8DF99EDF9D4D"),
            [OperationState.Error]      = new Guid("CC312205-1EF6-4A2E-A2A5-F2FB2CD25382")
        };
    }
    #endregion

    #region SibIntegrationUtilities
    public static class SibIntegrationUtilities
    {
        /// <summary>
        /// Получает значение колонки из выбранного объекта.
        /// </summary>
        /// <typeparam name="T">Тип колонки</typeparam>
        /// <param name="userConnection">UserConnection</param>
        /// <param name="lookupName">Название объекта</param>
        /// <param name="columnName">Название колонки</param>
        /// <param name="filterColumn">Название колонки, по которой выполняется фильтрация</param>
        /// <param name="filterValue">Значение для колонки фильтрации. Тип значения должен совпадать с типом колонки, указанной в 'filterColumn'</param>
        /// <returns>Значение колонки</returns>
        public static T GetLookupColumnValue<T>(UserConnection userConnection, string lookupName, string columnName, string filterColumn, object filterValue)
        {
            T columnValue = default(T);

            if (String.IsNullOrEmpty(lookupName) || String.IsNullOrEmpty(columnName) || String.IsNullOrEmpty(filterColumn) || filterValue == null ||
                (filterValue is Guid && (Guid)filterValue == Guid.Empty) || (filterValue is string && String.IsNullOrEmpty(filterValue.ToString())))
                return columnValue;

            var esq = new EntitySchemaQuery(userConnection.EntitySchemaManager, lookupName);
            esq.UseAdminRights = false;
            EntitySchemaQueryColumn column = esq.AddColumn(columnName);
            esq.Filters.Add(
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, filterColumn, filterValue)
            );
            var entityCollection = esq.GetEntityCollection(userConnection);
            if (entityCollection.Count() > 0) columnValue = entityCollection[0].GetTypedColumnValue<T>(column.Name);

            return columnValue;
        }

        /// <summary>
        /// Десереализует xml-файл в объект заданного типа.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого объекта</typeparam>
        /// <param name="fileName">Полное имя xml-файла</param>
        /// <param name="rootAttribute">Наименование корневого элемента xml-файла</param>
        /// <returns>Десериализованный объект</returns>
        public static T GetEntityFromXMLFile<T>(string fileName, string rootAttribute)
        {
            if (!System.IO.File.Exists(fileName))
                throw new FileNotFoundException(String.Format("Файл '{0}' не найден", fileName));

            T currentObject;
            XmlSerializer xs = new XmlSerializer(typeof(T), new XmlRootAttribute(rootAttribute));
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                currentObject = (T)xs.Deserialize(fs);
            }
            return currentObject;
        }

        /// <summary>
        /// Десереализует строку с xml в объект заданного типа.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого объекта</typeparam>
        /// <param name="xml">Строка с xml</param>
        /// <param name="rootAttribute">Наименование корневого элемента xml-файла</param>
        /// <returns>Десериализованный объект</returns>
        public static T GetEntityFromXMLString<T>(string xml, string rootAttribute)
        {
            T currentObject;
            XmlSerializer xs = new XmlSerializer(typeof(T), new XmlRootAttribute(rootAttribute));
            using (var stringReader = new StringReader(xml))
            {
                currentObject = (T)xs.Deserialize(stringReader);
            }

            return currentObject;
        }

        /// <summary>
        /// Десереализует base64 с xml в объект заданного типа. [Не отлажено]
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого объекта</typeparam>
        /// <param name="xml">Base64 с xml</param>
        /// <returns>Десериализованный объект</returns>
        public static T GetEntityFromBase64<T>(byte[] xml)
        {
            T currentObject;
            using (MemoryStream ms = new MemoryStream(xml))
            {
                IFormatter br = new BinaryFormatter();
                currentObject = (T)br.Deserialize(ms);
            }

            return currentObject;
        }

        /// <summary>
        /// Сериализует объект любого типа в base64.
        /// </summary>
        /// <param name="entity">Объект</param>
        /// <returns>Сериализованный объект</returns>
        public static byte[] ConvertToBase64(object entity)
        {
            if (entity == null) return null;
            var bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, entity);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Конвертирует base64binary (byte[]) в строку. 
        /// </summary>
        /// <param name="data">Данные в формате base64binary (byte[])</param>
        /// <returns>Результирующая строка</returns>
        public static string ConvertBase64ToString(byte[] data)
        {
            var xml = Encoding.UTF8.GetString(data);

            string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (xml[0] != '<' && xml.StartsWith(byteOrderMarkUtf8))
            {
                xml = xml.Remove(0, byteOrderMarkUtf8.Length);
            }

            return xml;
        }

        /// <summary>
        /// Получает из объекта пары ключей "BpmId"-"IntegrationId".
        /// </summary>
        /// <param name="userConnection">UserConnection</param>
        /// <param name="entityName">Название объекта</param>
        /// <param name="integrationIdColumnName">Название колонки, содержащей ключ внешней системы</param>
        /// <returns>Словарь, содержащий пары ключей "BpmId"-"IntegrationId"</returns>
        public static Dictionary<string, Guid> GetKeyPairsFromEntity(UserConnection userConnection,
            string entityName, string integrationIdColumnName)
        {
            var result = new Dictionary<string, Guid>();
            var esq = new EntitySchemaQuery(userConnection.EntitySchemaManager, entityName);
            esq.PrimaryQueryColumn.IsAlwaysSelect = true;
            EntitySchemaQueryColumn IntegrationIdColumn = esq.AddColumn(integrationIdColumnName);
            var entityCollection = esq.GetEntityCollection(userConnection);
            foreach (var entity in entityCollection)
            {
                var bpmId = entity.PrimaryColumnValue;
                var integrationId = entity.GetColumnValue(IntegrationIdColumn.Name)?.ToString() ?? "";
                if (!String.IsNullOrEmpty(integrationId) && !result.ContainsKey(integrationId)) result.Add(integrationId, bpmId);
            }
            return result;
        }

        /// <summary>
        /// Получает BpmId из пары ключей "BpmId"-"IntegrationId" по значению "IntegrationId".
        /// </summary>
        /// <param name="keyPairs">Объект с парами ключей "BpmId"-"IntegrationId"</param>
        /// <param name="integrationId">Значение ключа "IntegrationId"</param>
        /// <param name="isNotFound">Признак, указывающий, что запись с указанным значением "IntegrationId" не найдена в системе</param>
        /// <returns></returns>
        public static Guid GetBpmIdFromKeyPairs(Dictionary<string, Guid> keyPairs, string integrationId, out bool isNotFound)
        {
            var result = Guid.Empty;
            isNotFound = false;

            if (String.IsNullOrEmpty(integrationId)) return result;

            if (keyPairs.ContainsKey(integrationId))
            {
                result = keyPairs[integrationId];
            }
            else
            {
                isNotFound = true;
            }

            return result;
        }

        /// <summary>
        /// Получает IntegrationId из пары ключей "BpmId"-"IntegrationId" по значению "BpmId".
        /// </summary>
        /// <param name="keyPairs">Объект с парами ключей "BpmId"-"IntegrationId"</param>
        /// <param name="bpmId">Значение ключа "BpmId"</param>
        /// <param name="isNotFound">Признак, указывающий, что запись с указанным значением "IntegrationId" не найдена в системе</param>
        /// <returns></returns>
        public static string GetIntegrationIdFromKeyPairs(Dictionary<string, Guid> keyPairs, Guid bpmId, out bool isNotFound)
        {
            var result = String.Empty;
            isNotFound = false;

            if (bpmId == Guid.Empty) return result;

            var key = keyPairs.FirstOrDefault(x => x.Value == bpmId).Key;

            if (!String.IsNullOrEmpty(key))
            {
                isNotFound = false;
                result = key;
            }
            else
            {
                isNotFound = true;
            }

            return result;
        }

        /// <summary>
        /// Добавляет записи в заданный справочник в случае, если они там отсутствуют.
        /// </summary>
        /// <param name="userConnection">UserConnection</param>
        /// <param name="lookupName">Название объекта справочника</param>
        /// <param name="records">Коллекция записей</param>
        public static void UpdateLookupRecords(UserConnection userConnection, string lookupName, IEnumerable<string> records)
        {
            var lookupKeyPairs = GetKeyPairsFromEntity(userConnection, lookupName, "Name");
            foreach (var record in records)
            {
                if (GetBpmIdFromKeyPairs(lookupKeyPairs, record, out var isNotFound) == Guid.Empty)
                {
                    var schema = userConnection.EntitySchemaManager.GetInstanceByName(lookupName);
                    var lookupEntity = schema.CreateEntity(userConnection);
                    lookupEntity.SetDefColumnValues();
                    lookupEntity.SetColumnValue("Name", record);
                    lookupEntity.Save(false);
                }
            }
        }

        /// <summary>
        /// Добавляет запись в заданный справочник в случае, если они там отсутствуют.
        /// </summary>
        /// <param name="userConnection">UserConnection</param>
        /// <param name="lookupName">Название объекта справочника</param>
        /// <param name="record">Значение справочника</param>
        /// <returns>Id Записи</returns>
        public static Guid CreateLookupRecordIfNotExists(UserConnection userConnection, string lookupName, string record)
        {
            if (String.IsNullOrEmpty(record)) return Guid.Empty;

            var recordId = GetLookupColumnValue<Guid>(userConnection, lookupName, "Id", "Name", record);
            if (recordId == Guid.Empty)
            { 
                var schema = userConnection.EntitySchemaManager.GetInstanceByName(lookupName);
                var lookupEntity = schema.CreateEntity(userConnection);
                lookupEntity.SetDefColumnValues();
                lookupEntity.SetColumnValue("Name", record);
                lookupEntity.Save(false);
                return lookupEntity.PrimaryColumnValue;
            }
            else
            {
                return recordId;
            }
        }

        /// <summary>
        /// Добавляет элемент в List в случае, если он там отсутствует.
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="list">List</param>
        /// <param name="item">Добавляемый элемент</param>
        public static void AddItemToListIfNotExists<T>(List<T> list, T item)
        {
            if (typeof(T) == typeof(string) && String.IsNullOrEmpty(item as string) || object.Equals(item, default(T))) return;

            if (!list.Contains(item)) list.Add(item);
        }

        /// <summary>
        /// Объединяет два массива элементов типа T в один.
        /// </summary>
        /// <typeparam name="T">Тип элементов</typeparam>
        /// <param name="firstArray">Первый массив</param>
        /// <param name="secondArray">Второй массив</param>
        /// <returns>Результирующий массив</returns>
        public static T[] ConcatenateArrays<T>(T[] firstArray, T[] secondArray)
        {
            var resultList = new List<T>();
            if (firstArray != null) resultList.AddRange(firstArray);
            if (secondArray != null) resultList.AddRange(secondArray);
            return resultList.ToArray();
        }

        /// <summary>
        /// Создаёт привязку для соединения.
        /// </summary>
        /// <param name="url">Адрес</param>
        /// <param name="timeoutInMilliseconds">Таймаут, мс</param>
        /// <returns></returns>
        public static BasicHttpBinding CreateBinding(string url, int timeoutInMilliseconds, HttpClientCredentialType credentialType = HttpClientCredentialType.Basic)
        {
            var uri = new Uri(url);
            var uriSchema = uri.Scheme;
            BasicHttpBinding binding = new BasicHttpBinding();
            if (uriSchema == "https")
            {
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
                binding.Security.Transport.ClientCredentialType = credentialType;
            }
            binding.MaxReceivedMessageSize = 2147483647;
            binding.SendTimeout = TimeSpan.FromMilliseconds(timeoutInMilliseconds);

            return binding;
        }

        /// <summary>
        /// Создаёт объект 'X509ServiceCertificateAuthentication' с отключённой проверкой сертификата.
        /// </summary>
        /// <returns>Объект 'X509ServiceCertificateAuthentication'</returns>
        public static X509ServiceCertificateAuthentication GetCertificateAuthentication()
        {
            return new X509ServiceCertificateAuthentication
            {
                CertificateValidationMode = X509CertificateValidationMode.None
            };
        }

        /// <summary>
        /// Расчитывает оптимальное количество потоков для длительных операций.
        /// </summary>
        /// <returns>Количество потоков</returns>
        public static int GetOptimalThreadsCount()
        {
            int threadsCount = 4;
            try
            {
                var processorCount = Environment.ProcessorCount;
                if (processorCount <= 4) threadsCount = processorCount - 1;
                if (threadsCount < 2) threadsCount = 2;
            }
            catch (Exception)
            { }
            return threadsCount;
        }

        /// <summary>
        /// Отправляет сообщение странице на обновление
        /// </summary>
        /// <param name="recordId">Id записи</param>
        /// <param name="entityName">Название объекта</param>
        /// <returns></returns>
        public static void SendMessageUpdatePage(UserConnection userConnection, Guid recordId, string entityName)
        {
            try
            {
                var messageInfo = new
                {
                    userId = userConnection.CurrentUser.ContactId.ToString(),
                    action = "UpdatePage",
                    recordId = recordId.ToString()
                };

                Terrasoft.Configuration.MsgChannelUtilities.PostMessageToAll(
                    $"{entityName}Entity", JsonConvert.SerializeObject(messageInfo));
            }
            catch (Exception)
            { }
        }


    }
    #endregion


    #region RequiredParametersBehaviorAttribute and OptionalAttribute
    /// <summary>
    /// This WCF behavior changes metadata generation for service contracts, so that operation
    /// parameters are required by default (XML schema minOccurs="1").
    /// See http://thorarin.net/blog/post.aspx?id=5fe3b4b6-0e3e-463e-ac42-10c1c4808853 for
    /// a more thorough explanation.
    /// </summary>
    /// <remarks>
    /// Version 1.0 (2010-08-08):
    /// - Original release
    /// 
    /// Version 1.1 (2011-04-14):
    /// - Fixed a NullReferenceException that occurs when a service has two endpoints
    ///   configured that also use the same service contract. Thanks to Martin for reporting.
    /// </remarks>
    /// <example>
    /// The OptionalAttribute can be used to mark individual parameters as optional.
    /// <code>
    /// [ServiceContract]
    /// [RequiredParametersBehavior]
    /// public interface IGreetingService
    /// {
    ///     [OperationContract]
    ///     string Greet(string name, [Optional] string language);
    /// }
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Interface)]
	public class RequiredParametersBehaviorAttribute : Attribute, IContractBehavior, IWsdlExportExtension
	{
	    private List<RequiredMessagePart> _requiredParameter;
	
	    #region IContractBehavior Members (nothing to be done)
	
	    void IContractBehavior.AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
	    {
	    }
	
	    void IContractBehavior.ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
	    {
	    }
	
	    void IContractBehavior.ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
	    {
	    }
	
	    void IContractBehavior.Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
	    {
	    }
	
	    #endregion
	
	    #region IWsdlExportExtension Members
	
	    /// <summary>
	    /// When ExportContract is called to generate the necessary metadata, we inspect the service
	    /// contract and build a list of parameters that we'll need to adjust the XSD for later.
	    /// </summary>
	    void IWsdlExportExtension.ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
	    {
	        _requiredParameter = new List<RequiredMessagePart>();
	
	        foreach (var operation in context.Contract.Operations)
	        {
	            var inputMessage = operation.Messages.Where(m => m.Direction == System.ServiceModel.Description.MessageDirection.Input).First();
	            var parameters = operation.SyncMethod.GetParameters();
	            Debug.Assert(parameters.Length == inputMessage.Body.Parts.Count);
	
	            for (int i = 0; i < parameters.Length; i++)
	            {
	                object[] attributes = parameters[i].GetCustomAttributes(typeof(OptionalAttribute), false);
	                if (attributes.Length == 0)
	                {
	                    // The parameter has no [Optional] attribute, add it to the list of parameters
	                    // that we need to adjust the XML schema for later on.
	                    _requiredParameter.Add(new RequiredMessagePart()
	                    {
	                        Namespace = inputMessage.Body.Parts[i].Namespace,
	                        Message = operation.Name,
	                        Name = inputMessage.Body.Parts[i].Name
	                    });
	                }
	            }
	        }
	    }
	
	    /// <summary>
	    /// When ExportEndpoint is called, the XML schemas have been generated. Now we can manipulate to
	    /// our heart's content.
	    /// </summary>
	    void IWsdlExportExtension.ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
	    {
	        if (_requiredParameter == null)
	        {
	            // If we have defined two endpoints implementing the same contract within the same service,
	            // this method will be called twice. We only need to modify the schema once however.
	            return;
	        }
	
	        foreach (var p in _requiredParameter)
	        {
	            var schemas = exporter.GeneratedXmlSchemas.Schemas(p.Namespace);
	
	            foreach (XmlSchema schema in schemas)
	            {
	                var message = (XmlSchemaElement)schema.Elements[p.XmlQualifiedName];
	                var complexType = message.ElementSchemaType as XmlSchemaComplexType;
	                Debug.Assert(complexType != null, "Expected input message to be complex type.");
	                var sequence = complexType.Particle as XmlSchemaSequence;
	                Debug.Assert(sequence != null, "Expected a sequence.");
	
	                foreach (XmlSchemaElement item in sequence.Items)
	                {
	                    if (item.Name == p.Name)
	                    {
	                        item.MinOccurs = 1;
	                        item.MinOccursString = "1";
	                        break;
	                    }
	                }
	            }
	        }
	
	        // Throw away the temporary list we generated
	        _requiredParameter = null;
	    }
	
	    #endregion
	
	    #region Nested types
	
	    private class RequiredMessagePart
	    {
	        public string Namespace { get; set; }
	        public string Message { get; set; }
	        public string Name { get; set; }
	
	        public XmlQualifiedName XmlQualifiedName
	        {
	            get
	            {
	                return new XmlQualifiedName(Message, Namespace);
	            }
	        }
	    }
	
	    #endregion
	}
	
	[AttributeUsage(AttributeTargets.Parameter)]
	public class OptionalAttribute : Attribute
	{
	}
    #endregion
}