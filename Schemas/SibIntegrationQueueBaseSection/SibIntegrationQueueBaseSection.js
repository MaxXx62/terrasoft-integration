 define("SibIntegrationQueueBaseSection", ["SibIntegrationQueueBaseSectionResources", "ConfigurationEnums", "GridUtilitiesV2", "css!SibIntegrationQueueBaseSectionCss"],
    function(Resources, ConfigurationEnums) {
        return {
            entitySchemaName: "SibIntegrationQueueBase",
            messages: {},
            attributes: {
                "Direction": {
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
                },
                "ItemTypeColumnName": {
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
                },
                "UsedEntities": {
					dataValueType: Terrasoft.DataValueType.COLLECTION,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
					/*
					value: {
						"Контрагент": {
							entityName: "Account",
							pageName: "AccountPageV2",
                            recordIdColumn: "RecordId",
							integrationIdEntityColumn: "SibMDMIntegrationId",
							getCustomFilter: function(integrationId, scope) {
								return scope.getMDMFilter(integrationId);
							}
						},
						...
					}
					*/
				},
                "FilterIncomplete": {
                    dataValueType: Terrasoft.DataValueType.BOOLEAN,
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    value: true,
                    onChange: "sibReloadGrid"
                },
            },
            mixins: {},
            methods: {
                /*
                * overriden
                */
                init: function() {
                    this.callParent(arguments);
                    this.set("IsIncludeInFolderButtonVisible", false);
                },

                /*
                * overriden
                */
                initFixedFiltersConfig: function() {
                    var fixedFilterConfig = {
                        entitySchema: this.entitySchema,
                        filters: [
                            {
                                name: "PeriodFilter",
                                caption: this.get("Resources.Strings.PeriodFilterCaption"),
                                dataValueType: this.Terrasoft.DataValueType.DATE,
                                startDate: {
                                    columnName: "SibModifiedOn",
                                    defValue: this.Terrasoft.startOfWeek(new Date())
                                },
                                dueDate: {
                                    columnName: "SibModifiedOn",
                                    defValue: this.Terrasoft.endOfWeek(new Date())
                                }
                            },
                        ]
                    };
                    this.set("FixedFilterConfig", fixedFilterConfig);
                },

                /*
                * overriden
                */
                initQueryFilters: function(esq) {
                    var integrationStatus__Complete = '7f8338b5-c476-4389-9cf8-8df99edf9d4d';

                    var sectionFilters = this.get("SectionFilters");
                    var filterIncomplete = this.get("FilterIncomplete");

                    sectionFilters.removeByKey("FilterIncomplete");

                    if (filterIncomplete) {
                        sectionFilters.add("FilterIncomplete", Terrasoft.createColumnFilterWithParameter(
    					    Terrasoft.ComparisonType.NOT_EQUAL, "SibIntegrationStatus", integrationStatus__Complete));
                    }
                    this.callParent(arguments);
    			},

                /*
                * overriden
                */
                addSectionDesignerViewOptions: this.Terrasoft.emptyFn,
                addSectionHistoryState: this.Terrasoft.emptyFn,
                removeSectionHistoryState: this.Terrasoft.emptyFn,
                initRunProcessButtonMenu: this.Terrasoft.emptyFn,

                /*
                * overriden
                */
                addCardHistoryState: function(schemaName, operation, primaryColumnValue) {
                    if (!schemaName) {
                        return;
                    }
                    var cardOperationConfig = {
                        schemaName: schemaName,
                        operation: operation,
                        primaryColumnValue: primaryColumnValue
                    };
                    var historyState = this.getHistoryStateInfo();
                    var stateConfig = this.getCardHistoryStateConfig(cardOperationConfig);
                    var eventName = (historyState.workAreaMode === ConfigurationEnums.WorkAreaMode.COMBINED)
                        ? "ReplaceHistoryState"
                        : "PushHistoryState";
                    this.sandbox.publish(eventName, stateConfig);
                },

                /*
                * overriden
                */
                removeCardHistoryState: function () {
                    var module = "SectionModuleV2";
                    var schema = this.name;
                    var historyState = this.sandbox.publish("GetHistoryState");
                    var currentState = historyState.state;
                    var newState = {
                        moduleId: currentState.moduleId
                    };
                    var hash = [module, schema].join("/");
                    this.sandbox.publish("PushHistoryState", {
                        hash: hash,
                        stateObj: newState,
                        silent: true
                    });
                },

                /*
                * overriden
                */
                getDefaultDataViews: function() {
                    var dataViews = this.callParent(arguments);
                    delete dataViews.AnalyticsDataView;
                    return dataViews;
                },

                /*
                * overriden
                */
                closeSection: function() {
                    var module = "SystemDesigner";
                    this.sandbox.publish("PushHistoryState", {
                        hash: this.Terrasoft.combinePath("IntroPage", module)
                    });
                },

                /*
                * overriden
                */
                editRecord: function(primaryColumnValue) {
                    var activeRow = this.getActiveRow();
                    var typeColumnValue = this.getTypeColumnValue(activeRow);
                    var schemaName = this.getEditPageSchemaName(typeColumnValue);
                    this.set("ShowCloseButton", true);
                    this.openCardInChain({
                        id: primaryColumnValue,
                        schemaName: schemaName,
                        operation: ConfigurationEnums.CardStateV2.EDIT,
                        moduleId: this.getChainCardModuleSandboxId(typeColumnValue),
                        instanceConfig: {
                            useSeparatedPageHeader: this.get("UseSeparatedPageHeader")
                        }
                    });
                },

                /*
                * overriden
                */
                onActiveRowAction: function(buttonTag, primaryColumnValue) {
                    if (buttonTag == 'gotoRecord') {
                        var gridData = this.getGridData();
	                    var activeRow = gridData.get(primaryColumnValue);
	                    var recordType = (activeRow.get(this.get("ItemTypeColumnName")) && activeRow.get(this.get("ItemTypeColumnName")).displayValue) ||
                            activeRow.get(this.get("ItemTypeColumnName"));
                        var recordId = activeRow.get(this.get("UsedEntities")[recordType].recordIdColumn);
	                    var pageSchemaName = this.get("UsedEntities")[recordType].pageName;
	                    var entitySchemaName = this.get("UsedEntities")[recordType].entityName;
						var integrationIdColumnName = this.get("UsedEntities")[recordType].integrationIdColumn;
						var filter = this.get("UsedEntities")[recordType].getCustomFilter;
	                    if (!pageSchemaName || !entitySchemaName) return;

                        if (this.get("Direction") == "Экспорт") {
	                        esq = this.checkRecordExistsEsq(entitySchemaName, recordId);
	                        esq.getEntityCollection(function(response) {
	                            if(response && response.success && response.collection.getCount() > 0) {
	                                this.sandbox.publish("PushHistoryState",
										{hash: Terrasoft.combinePath("CardModuleV2", pageSchemaName, "edit", recordId)}
									);
	                            } else {
	                                this.showInformationDialog(
										Ext.String.format(this.get("Resources.Strings.GoToRecordErrorMessage"), "Id", recordId)
									);
	                            }
	                        }, this);
	                    } else {
	                        esq = this.getRecordByIntegrationIdEsq(entitySchemaName, recordId, integrationIdColumnName, filter);
	                        esq.getEntityCollection(function(response) {
	                            if (response && response.success && response.collection.getCount() > 0) {
	                                var id = response.collection.getByIndex(0).get("Id");
	                                this.sandbox.publish("PushHistoryState",
										{hash: Terrasoft.combinePath("CardModuleV2", pageSchemaName, "edit", id)}
									);
	                            } else {
	                                this.showInformationDialog(this.get("Resources.Strings.GoToRecordSimpleErrorMessage"));
	                            }
	                        }, this);
	                    }
                    } else if (buttonTag == 'edit') {
						this.editRecord(primaryColumnValue);
					} else {
                        this.callParent(arguments);
                    }
                },

                checkRecordExistsEsq: function (entitySchemaName, id) {
					esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: entitySchemaName
					});
					esq.addColumn("Id");
					esq.filters.add("IdFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", id));
					return esq;
				},

				getRecordByIntegrationIdEsq: function (entitySchemaName, integrationId, integrationIdColumnName, filter) {
					esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: entitySchemaName
					});
					esq.addColumn("Id");
					if (!filter) {
						esq.filters.add("IntegrationIdFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, integrationIdColumnName, integrationId));
					} else {
						esq.filters.add(filter(integrationId, this));
					}
					return esq;
				},

                sibReloadGrid: function(a) {
                    this.set("IsClearGridData", true);
                    this.afterFiltersUpdated();
                },
            },
            diff: /**SCHEMA_DIFF*/[
                {
                    "operation": "merge",
                    "name": "SeparateModeAddRecordButton",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "DataGridActiveRowOpenAction",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "DataGridActiveRowCopyAction",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "CombinedModeAddRecordButton",
                    "values": {
                        "visible": false
                    }
                },
                {
	                "operation": "insert",
	                "name": "EditButton",
	                "parentName": "DataGrid",
	                "propertyName": "activeRowActions",
	                "values": {
	                    "id": "EditActionButton",
	                    "className": "Terrasoft.Button",
	                    "style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
	                    "visible": true,
	                    "caption": Resources.localizableStrings.EditButtonCaption,
	                    "tag": "edit"
	                }
	            },
                {
                    "operation": "insert",
                    "name": "GoToRecordButton",
                    "parentName": "DataGrid",
                    "propertyName": "activeRowActions",
                    "values": {
                        "id": "GoToRecordActionButton",
                        "className": "Terrasoft.Button",
                        "style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
                        "visible": true,
                        "caption": Resources.localizableStrings.GoToButtonCaption,
                        "tag": "gotoRecord"
                    }
                },
                {
                    "operation": "insert",
                    "name": "SeparateModeBackButton",
                    "parentName": "SeparateModeActionButtonsLeftContainer",
                    "propertyName": "items",
                    "index": 2,
                    "values": {
                        "itemType": this.Terrasoft.ViewItemType.BUTTON,
                        "caption": {"bindTo": "Resources.Strings.BackButtonCaption"},
                        "click": {"bindTo": "closeSection"},
                        "classes": {
                            "textClass": ["actions-button-margin-right"],
                            "wrapperClass": ["actions-button-margin-right"]
                        },
                        "visible": {"bindTo": "SeparateModeActionsButtonVisible"}
                    }
                },
                {
					"operation": "insert",
					"name": "SibFiltersContainer",
					"parentName": "LeftGridUtilsContainer",
					"propertyName": "items",
					"values": {
						"generateId": "SibFiltersContainer",
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"wrapClass": ["sib-filtersContainer-class"],
						"items": []
					},
					"index": 1
				},
                {
					"operation": "insert",
					"name": "FilterIncompleteContainer",
					"parentName": "SibFiltersContainer",
					"propertyName": "items",
					"values": {
						"Id": "FilterIncompleteContainer",
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"wrapClass": ["sib-filtersContainer-class"],
						"items": []
					},
					"index": 1
				},
				{
					"operation": "insert",
					"name": "FilterIncompleteCaption",
					"parentName": "FilterIncompleteContainer",
					"propertyName": "items",
					"values": {
						"itemType": Terrasoft.ViewItemType.LABEL,
						"classes": {
							"labelClass": ["sib-filtersContainer-label-class"]
						},
						"caption": {
							"bindTo": "Resources.Strings.FilterIncompleteCaption"
						},
					},
					"index": 0
				},
				{
					"operation": "insert",
					"name": "IsFilterIncomplete",
					"parentName": "FilterIncompleteContainer",
					"propertyName": "items",
					"values": {
						"dataValueType": Terrasoft.DataValueType.BOOLEAN,
						"bindTo": "FilterIncomplete",
						"labelConfig": {
							"visible": false
						},
						"classes": {
							"wrapClassName": ["sib-filtersContainer-checkboxedit-class"]
						},
						"controlConfig": {
							"className": "Terrasoft.CheckBoxEdit"
						}
					},
					"index": 1
				},
            ]/**SCHEMA_DIFF*/
        };
    });
