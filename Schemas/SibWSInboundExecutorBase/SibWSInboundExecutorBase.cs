using Terrasoft.Core;

namespace SiblionIntegration
{
    public abstract class SibWSInboundExecutorBase: SibWSExecutorBase
    {
        protected System.Exception RaiseException { get; set; }

        public SibWSInboundExecutorBase(UserConnection userConnection, ServiceInfo serviceInfo): base (userConnection, serviceInfo)
        {
        }

        public override object Execute()
        {
            OptionalOverrideServiceInfo();

            var parameter = Parameters["InboundObject"];
            var message = (Parameters.ContainsKey("InboundMessage")) ? Parameters["InboundMessage"] : null;
            CreatePrimaryLogRecord();
            SaveInboundMessageToPrimaryLog(message ?? parameter);

            var response = ExecuteServiceLogic(parameter);

            SaveOutboundMessageToPrimaryLog(response);
            ClosePrimaryLogRecord();
            if (RaiseException != null) throw RaiseException;

            return response;
        }

        protected abstract object ExecuteServiceLogic(object parameter);
    }
}