using System;
using Terrasoft.Core;

namespace SiblionIntegration
{
    public abstract class SibWSOutboundGetExecutorBase: SibWSOutboundExecutorBase
    {
        public SibWSOutboundGetExecutorBase(UserConnection userConnection, ServiceInfo serviceInfo) : base(userConnection, serviceInfo)
        {
        }

        public override object Execute()
        {
            OptionalOverrideServiceInfo();

            CreatePrimaryLogRecord();

            object serviceParameter = null, serviceResponse = null, processDataResponse = null;

            try
            {
                try
                {
                    serviceParameter = PrepareData();
                }
                catch(Exception e)
                {
                    throw new WSOutboundPrepareDataException("Ошибка формирования входных данных исходящего веб-сервиса.", e);
                }
                SaveOutboundMessageToPrimaryLog(serviceParameter);

                try
                {
                    SaveExternalStartTimeToLog();
                    serviceResponse = CallWebService(serviceParameter);
                }
                catch (Exception e)
                {
                    throw new WSOutboundCallWebServiceException("Ошибка вызова исходящего веб-сервиса.", e);
                }
                finally
                {
                    SaveExternalEndTimeToLog();
                }
                SaveInboundMessageToPrimaryLog(serviceResponse);


                try
                {
                    processDataResponse = ProcessData(serviceResponse);
                }
                catch (WSExpectedException e)
                {
                    throw;
                }
                catch (WSBusinessException e)
                {
                    throw;
                }
                catch (WSExternalSystemException e)
                {
                    throw;
                }
                catch (Exception e)
                {
                    throw new WSOutboundProcessDataException("Ошибка обработки данных, полученных от веб-сервиса.", e);
                }
            }
            catch (WSExpectedException e)
            {
                ErrorException = e;
            }
            catch (WSBusinessException e)
            {
                ErrorException = e;
                throw;
            }
            catch (Exception e)
            {
                ErrorException = e;
                throw;
            }
            finally
            {
                ClosePrimaryLogRecord();
            }

            return new ServiceResult
            {
                IsSuccess = (ErrorException == null || ErrorException.GetType() == typeof(WSExpectedException)),
                Result = processDataResponse
            };
        }
    }
}