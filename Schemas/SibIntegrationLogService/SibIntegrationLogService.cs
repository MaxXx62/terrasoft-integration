using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using global::Common.Logging;

namespace SiblionIntegration
{
    #region SibIntegrationLogService
    public abstract class SibIntegrationLogService
    {
        

        

        protected const int MAX_LOG_LEVEL = 3;
        protected const int MAX_STORAGE_METHOD = 1;
        protected enum LevelOfLogging { ErrorsOnly, ErrorsWithXml, AllWithErrorXml, AllWithXml };
        protected enum StorageMethod { Database, FileSystem };

        public static Dictionary<Guid, SibIntegrationLogService> CurrentLogs;
        public static object CurrentLogsLock = new object();

        protected UserConnection _userConnection;
        protected int _logLevel;
        public int LogLevel
        {
            get => _logLevel;
            set
            {
                _logLevel = value;
                _logLevel = _logLevel <= MAX_LOG_LEVEL ? _logLevel : MAX_LOG_LEVEL;
                _logLevel = _logLevel >= 0 ? _logLevel : 0;
            }
        }

        protected int _messageStorageMethod;
        protected StorageMethod MessageStorageMethod
        {
            get => (StorageMethod)_messageStorageMethod;
            set
            {
                _messageStorageMethod = (int)value;
                _messageStorageMethod = _messageStorageMethod > MAX_STORAGE_METHOD || _messageStorageMethod < 0 ? 0 : _messageStorageMethod;
            }
        }

        protected string StoragePath { get; set; }

        public int XMLMaxLength = 300000;
        public Guid PrimaryLogId { get; private set; }
        public Entity PrimaryLogEntity { get; private set; }
        protected Dictionary<Guid, Entity> ChildLogEntities { get; set; } = new Dictionary<Guid, Entity>();
        protected object ChildLogEntitiesLock = new object();
        protected object ChildRecordCountLock = new object();


        protected abstract string PrimaryEntityName { get; }
        protected abstract string ChildEntityName { get; }
        protected abstract string LogLevelSysPrefName { get; }
        protected abstract string StorageMethodSysPrefName { get; }
        protected abstract string StoragePathSysPrefName { get; }


        static SibIntegrationLogService()
        {
            CurrentLogs = new Dictionary<Guid, SibIntegrationLogService>();
        }

        public static SibIntegrationLogService CreateLogService(string serviceName, UserConnection userConnection)
        {
            if (String.IsNullOrEmpty(serviceName)) throw new ArgumentNullException(nameof(serviceName));

            var logServiceType = Type.GetType(serviceName);
            if (logServiceType == null) throw new Exception($"Тип '{serviceName}' не найден.");

            return (SibIntegrationLogService)Activator.CreateInstance(logServiceType, userConnection);
        }

        public static SibIntegrationLogService GetExistingLogService(Guid id)
        {
            return CurrentLogs.TryGetValue(id, out var value) ? value : null;
        }

        protected SibIntegrationLogService(UserConnection userConnection)
        {
            _userConnection = userConnection;

            Terrasoft.Core.Configuration.SysSettings.TryGetValue(_userConnection, LogLevelSysPrefName, out var logLevelSysPref);
            _logLevel = logLevelSysPref != null ? Convert.ToInt32(logLevelSysPref.ToString()) : 0;

            _logLevel = _logLevel <= MAX_LOG_LEVEL ? _logLevel : MAX_LOG_LEVEL;
            _logLevel = _logLevel >= 0 ? _logLevel : 0;

            if (!String.IsNullOrEmpty(StorageMethodSysPrefName))
            {
                Terrasoft.Core.Configuration.SysSettings.TryGetValue(_userConnection, StorageMethodSysPrefName,
                    out var storageMethodSysPref);
                _messageStorageMethod =
                    storageMethodSysPref != null ? Convert.ToInt32(storageMethodSysPref.ToString()) : 0;

                _messageStorageMethod = _messageStorageMethod > MAX_STORAGE_METHOD || _messageStorageMethod < 0
                    ? 0
                    : _messageStorageMethod;
            }

            if (!String.IsNullOrEmpty(StoragePathSysPrefName))
            {
                Terrasoft.Core.Configuration.SysSettings.TryGetValue(_userConnection, StoragePathSysPrefName,
                    out var storagePathSysPref);
                StoragePath = storagePathSysPref != null ? storagePathSysPref.ToString() : String.Empty;
            }
        }


        public Guid CreateLogRecord(string name, string code, string direction)
        {
            Guid result = Guid.Empty;

            try
            {
                EntitySchema schema = _userConnection.EntitySchemaManager.GetInstanceByName(PrimaryEntityName);
                Entity entity = schema.CreateEntity(_userConnection);
                entity.SetDefColumnValues();
                entity.SetColumnValue("Name", name);
                entity.SetColumnValue("Code", code);
                entity.SetColumnValue("Direction", direction);
                entity.SetColumnValue("LogLevel", _logLevel);
                entity.SetColumnValue("MessagesStorageMethod", _messageStorageMethod);
                if (entity.Save(false))
                {
                    result = entity.GetTypedColumnValue<Guid>("Id");
                    PrimaryLogEntity = entity;
                }
            }
            catch (Exception e) { }

            PrimaryLogId = result;

            if (PrimaryLogId != Guid.Empty)
            {
                lock (CurrentLogsLock)
                {
                    CurrentLogs.Add(PrimaryLogId, this);
                }
            }

            return result;
        }

        public Guid CreateVirtualLogItemRecord(string recordId, string recordType, string direction)
        {
            Guid result = Guid.Empty;
            try
            {
                EntitySchema schema = _userConnection.EntitySchemaManager.GetInstanceByName(ChildEntityName);
                Entity entity = schema.CreateEntity(_userConnection);
                entity.SetDefColumnValues();
                entity.SetColumnValue("CreatedOn", DateTime.UtcNow);
                entity.SetColumnValue("RecordId", recordId);
                entity.SetColumnValue("RecordType", recordType);
                entity.SetColumnValue("Direction", direction);
                entity.SetColumnValue("MessagesStorageMethod", _messageStorageMethod);
                entity.SetColumnValue("SibIntegrationLogId", PrimaryLogId);

                result = entity.PrimaryColumnValue;
                lock (ChildLogEntitiesLock)
                {
                    ChildLogEntities.Add(result, entity);
                }
            }
            catch (Exception e) { }
            return result;
        }

        public void SetRecordCountToLog(int recordCount, bool isAdd = false)
        {
            if (PrimaryLogEntity == null) return;
            lock (ChildRecordCountLock)
            {
                try
                {
                    PrimaryLogEntity.SetColumnValue("RecordCount",
                        isAdd ? PrimaryLogEntity.GetTypedColumnValue<int>("RecordCount") + recordCount : recordCount);
                    PrimaryLogEntity.Save(false);
                }
                catch (Exception) { }
            }
        }

        public void SetExternalTimeToLog(bool isStart)
        {
            if (PrimaryLogEntity == null) return;
            var columnName = isStart ? "ExternalStartedOn" : "ExternalEndedOn";
            PrimaryLogEntity.SetColumnValue(columnName, DateTime.UtcNow);
        }

        public void SetExternalTimeToVirtualLogItem(Guid childLogId, bool isStart)
        {
            var childLogEntity = GetChildLogEntity(childLogId);
            var columnName = isStart ? "ExternalStartedOn" : "ExternalEndedOn";
            childLogEntity?.SetColumnValue(columnName, DateTime.UtcNow);
        }

        public void SetOutboundMessageToLog(object messageObject)
        {
            switch (MessageStorageMethod)
            {
                case StorageMethod.Database:
                {
                    SetWSMessageToDatabaseLog("OutboundMessage", messageObject);
                    break;
                }
                case StorageMethod.FileSystem:
                {
                    var fileName = SetWSMessageToFileSystemLog("OutMessage", PrimaryLogId, messageObject);
                    PrimaryLogEntity.SetColumnValue("OutboundMessage", fileName);
                    PrimaryLogEntity.Save(false);
                    break;
                }
            }
        }

        public void SetInboundMessageToLog(object messageObject)
        {
            switch (MessageStorageMethod)
            {
                case StorageMethod.Database:
                {
                    SetWSMessageToDatabaseLog("InboundMessage", messageObject);
                    break;
                }
                case StorageMethod.FileSystem:
                {
                    var fileName = SetWSMessageToFileSystemLog("InMessage", PrimaryLogId, messageObject);
                    PrimaryLogEntity.SetColumnValue("InboundMessage", fileName);
                    PrimaryLogEntity.Save(false);
                    break;
                }
            }
        }

        public void SetOutboundMessageToVirtualLogItem(Guid childLogId, object messageObject)
        {
            SetWSMessageToVirtualLogItem(childLogId, "OutboundMessage", messageObject);
        }

        public void SetInboundMessageToVirtualLogItem(Guid childLogId, object messageObject)
        {
            SetWSMessageToVirtualLogItem(childLogId, "InboundMessage", messageObject);
        }

        public void CloseLogRecord(Exception errorException = null, string description = null, bool withDeleteOption = false)
        {
            if (PrimaryLogEntity == null) return;
            try
            {
                var entity = PrimaryLogEntity;
                if (entity != null)
                {
                    if (withDeleteOption && errorException == null && entity.GetTypedColumnValue<int>("RecordCount") == 0)
                    {
                        entity.Delete();
                    }
                    else
                    {
                        entity.SetColumnValue("EndedOn", DateTime.UtcNow);
                        if (errorException == null)
                        {
                            entity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Complete]);
                        }
                        else if (errorException.GetType() == typeof(WSExpectedException) ||
                             errorException.GetType() == typeof(WSBusinessException))
                        {
                            entity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Complete]);
                            entity.SetColumnValue("ErrorText", errorException.Message);
                        }
                        else
                        {
                            entity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Error]);
                            entity.SetColumnValue("ErrorText", errorException.ToString());
                        }

                        if (!String.IsNullOrEmpty(description)) entity.SetColumnValue("Description", description);
                        entity.Save(false);
                    }
                }
            }
            catch (Exception e) { }
            finally
            {
                lock (CurrentLogsLock)
                {
                    CurrentLogs.Remove(PrimaryLogId);
                }
            }
        }

        public void SaveVirtualLogItemRecord(Guid childLogId, Exception errorException = null)
        {
            if ((_logLevel == (int)LevelOfLogging.ErrorsOnly || _logLevel == (int)LevelOfLogging.ErrorsWithXml) && errorException == null)
            {
                lock (ChildLogEntitiesLock)
                {
                    ChildLogEntities.Remove(childLogId);
                }

                return;
            }

            try
            {
                var childLogEntity = GetChildLogEntity(childLogId);
                if (childLogEntity != null)
                {
                    if (errorException == null)
                    {
                        childLogEntity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Complete]);
                        if (_logLevel != (int)LevelOfLogging.AllWithXml)
                        {
                            childLogEntity.SetColumnValue("OutboundMessage", null);
                            childLogEntity.SetColumnValue("InboundMessage", null);
                        }
                    }
                    else if (errorException.GetType() == typeof(WSExpectedException) || errorException.GetType() == typeof(WSBusinessException))
                    {
                        childLogEntity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Complete]);
                        childLogEntity.SetColumnValue("ErrorText", errorException.Message);
                        if (_logLevel == (int)LevelOfLogging.ErrorsOnly)
                        {
                            childLogEntity.SetColumnValue("OutboundMessage", null);
                            childLogEntity.SetColumnValue("InboundMessage", null);
                        }
                    }
                    else
                    {
                        childLogEntity.SetColumnValue("SibIntegrationStatusId", SibIntegrationConstants.OperationStates[OperationState.Error]);
                        childLogEntity.SetColumnValue("ErrorText", errorException.ToString());
                        if (_logLevel == (int)LevelOfLogging.ErrorsOnly)
                        {
                            childLogEntity.SetColumnValue("OutboundMessage", null);
                            childLogEntity.SetColumnValue("InboundMessage", null);
                        }
                    }

                    if (MessageStorageMethod == StorageMethod.FileSystem)
                    {
                        var outboundMessageString = childLogEntity.GetTypedColumnValue<string>("OutboundMessage");
                        var inboundMessageString = childLogEntity.GetTypedColumnValue<string>("InboundMessage");
                        if (!String.IsNullOrEmpty(outboundMessageString))
                        {
                            var fileName = SetWSMessageToFileSystemLog("OutMessage", childLogId, outboundMessageString);
                            childLogEntity.SetColumnValue("OutboundMessage", fileName);
                        }

                        if (!String.IsNullOrEmpty(inboundMessageString))
                        {
                            var fileName = SetWSMessageToFileSystemLog("InMessage", childLogId, inboundMessageString);
                            childLogEntity.SetColumnValue("InboundMessage", fileName);
                        }
                    }

                    childLogEntity.SetColumnValue("ModifiedOn", DateTime.UtcNow);
                    childLogEntity.Save(false);
                }
            }
            catch (Exception e) { }
            finally
            {
                lock (ChildLogEntitiesLock)
                {
                    ChildLogEntities.Remove(childLogId);
                }
            }
        }

        public void CreateAndCloseLogItemRecord(string recordId, string recordType, string direction, object requestObject, object responseObject,
            Exception errorException = null)
        {
            if ((_logLevel == (int)LevelOfLogging.ErrorsOnly || _logLevel == (int)LevelOfLogging.ErrorsWithXml) && errorException == null) return;
            try
            {
                EntitySchema schema = _userConnection.EntitySchemaManager.GetInstanceByName(ChildEntityName);
                Entity entity = schema.CreateEntity(_userConnection);
                entity.SetDefColumnValues();
                entity.SetColumnValue("SibIntegrationLogId", PrimaryLogId);
                entity.SetColumnValue("RecordType", recordType);
                entity.SetColumnValue("RecordId", recordId);
                entity.SetColumnValue("Direction", direction);
                entity.SetColumnValue("MessagesStorageMethod", _messageStorageMethod);

                var stateId = (errorException == null || errorException.GetType() == typeof(WSExpectedException) || errorException.GetType() == typeof(WSBusinessException) ?
                    SibIntegrationConstants.OperationStates[OperationState.Complete] : SibIntegrationConstants.OperationStates[OperationState.Error]);
                entity.SetColumnValue("SibIntegrationStatusId", stateId);
                entity.SetColumnValue("ErrorText", errorException?.ToString());
                if ((_logLevel > (int)LevelOfLogging.ErrorsOnly && errorException != null) || _logLevel == (int)LevelOfLogging.AllWithXml)
                {
                    switch (MessageStorageMethod)
                    {
                        case StorageMethod.Database:
                        {
                            entity.SetColumnValue("OutboundMessage", SibIntegrationLogUtilities.ConvertObjectToXMLString(requestObject, XMLMaxLength));
                            entity.SetColumnValue("InboundMessage", SibIntegrationLogUtilities.ConvertObjectToXMLString(responseObject, XMLMaxLength));
                            break;
                        }
                        case StorageMethod.FileSystem:
                        {
                            var fileName = SetWSMessageToFileSystemLog("OutMessage", entity.PrimaryColumnValue, requestObject);
                            entity.SetColumnValue("OutboundMessage", fileName);

                            fileName = SetWSMessageToFileSystemLog("InMessage", entity.PrimaryColumnValue, responseObject);
                            entity.SetColumnValue("InboundMessage", fileName);
                            break;
                        }
                    }
                }
                entity.Save(false);
            }
            catch (Exception e) { }
        }

        public Entity GetChildLogEntity(Guid id)
        {
            Entity childLogEntity;
            bool isSuccess;
            lock (ChildLogEntitiesLock)
            {
                isSuccess = ChildLogEntities.TryGetValue(id, out childLogEntity);
            }
            return isSuccess ? childLogEntity : null;
        }

        protected void SetWSMessageToDatabaseLog(string columnName, object messageObject)
        {
            if (PrimaryLogEntity == null) return;

            try
            {
                PrimaryLogEntity.SetColumnValue(columnName, SibIntegrationLogUtilities.ConvertObjectToXMLString(messageObject, XMLMaxLength));
                PrimaryLogEntity.Save(false);
            }
            catch (Exception) { }
        }

        protected string SetWSMessageToFileSystemLog(string prefix, Guid recordId, object messageObject)
        {
            if (PrimaryLogEntity == null || messageObject == null) return null;
            if (String.IsNullOrEmpty(StoragePath) || !Directory.Exists(StoragePath)) return null;

            try
            {
                var fileName = GetFileName(prefix, recordId);
                if (messageObject is string contents)
                    File.WriteAllText(fileName, contents);
                else
                    File.WriteAllText(fileName,
                        SibIntegrationLogUtilities.ConvertObjectToXMLString(messageObject, XMLMaxLength));

                return fileName;
            }
            catch (Exception e)
            {
                SibIntegrationLogger.Error(this.GetType().Name, e.Message);
            }

            return null;
        }

        protected void SetWSMessageToVirtualLogItem(Guid childLogId, string columnName, object messageObject)
        {
            if (_logLevel > (int)LevelOfLogging.ErrorsOnly && childLogId != Guid.Empty)
            {
                try
                {
                    var childLogEntity = GetChildLogEntity(childLogId);
                    childLogEntity?.SetColumnValue(columnName, SibIntegrationLogUtilities.ConvertObjectToXMLString(messageObject, XMLMaxLength));
                }
                catch (Exception) { }
            }
        }

        private string GetFileName(string prefix, Guid recordId)
        {
            var workPath = StoragePath + DateTime.Now.ToString("yyyyMMdd");
            if (!Directory.Exists(workPath)) Directory.CreateDirectory(workPath);
            return workPath + "\\" + prefix + "_" + recordId.ToString() + ".xml";
        }
    }
    #endregion

    #region SibIntegrationLogUtilities
    public static class SibIntegrationLogUtilities
    {
        public static string ConvertObjectToXMLString(object currentObject, int maxLength)
        {
            string resultString = String.Empty;

            if (currentObject == null ||
                (currentObject.GetType().IsArray) && ((IList)currentObject).Count == 0) return resultString;

            if (currentObject.GetType().IsPrimitive)
            {
                resultString = CreateXMLFromSimpleParameter(currentObject.ToString());
            }
            else if (currentObject.GetType().IsArray && currentObject.GetType().GetElementType().IsPrimitive &&
                currentObject.GetType().GetElementType() != typeof(byte))
            {
                foreach (var item in currentObject as IEnumerable)
                {
                    resultString += item.ToString() + " ";
                }
            }
            else if (currentObject.GetType().IsArray && currentObject.GetType().GetElementType() == typeof(byte))
            {
                resultString = CreateXMLFromSimpleParameter(Convert.ToBase64String(currentObject as byte[]));
            }
            else
            {
                var stringWriter = new Utf8StringWriter();
                XmlSerializer xs;
                if (typeof(ITypeWithAlternativeSerializer).IsAssignableFrom(currentObject.GetType()))
                {
                    xs = (currentObject as ITypeWithAlternativeSerializer).GetAlternativeSerializer() ?? new XmlSerializer(currentObject.GetType());
                    xs.Serialize(stringWriter, currentObject);
                }
                else
                {
                    xs = new XmlSerializer(currentObject.GetType());
                    xs.Serialize(stringWriter, currentObject);
                }

                resultString = stringWriter.ToString();
            }

            return (maxLength <= 0)
                ? resultString
                : resultString.Substring(0, resultString.Length <= maxLength ? resultString.Length : maxLength);
        }

        public static string CreateXMLFromSimpleParameter(string parameter)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement el = (XmlElement)doc.AppendChild(doc.CreateElement("Body"));
            el.SetAttribute("Parameter", parameter);
            return doc.OuterXml;
        }

        //Логгирование объектов в xml-файл
        public static void LogToFile<T>(string stream, string direction, T currentObject)
        {
            try
            {
                var logDirectory = Path.Combine(Directory.GetParent(System.Web.HttpRuntime.AppDomainAppPath.TrimEnd('\\')).FullName, "IntegrLog");
                if (!Directory.Exists(logDirectory)) Directory.CreateDirectory(logDirectory);
                var logFileName = logDirectory + "\\" + stream + "_" + direction + "_" + DateTime.Now.ToString("yyyyMMddHHmmss_fff") + ".xml";

                XmlSerializer xs = new XmlSerializer(typeof(T));
                using (TextWriter tw = new StreamWriter(logFileName))
                {
                    xs.Serialize(tw, currentObject);
                }
            }
            catch (Exception) { }
        }

        public static void LogInfoToFile(string fileName, string line)
        {
            File.AppendAllText(fileName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + line + Environment.NewLine);
        }

        private sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }
    }
    #endregion

    #region SiblionIntegrationLogger
    public static class SibIntegrationLogger
    {
        private static readonly ILog _log = LogManager.GetLogger("SiblionIntegration");

        public static void Info(string message)
        {
            _log.Info(message);
        }

        public static void Info(string description, string message)
        {
            Info($"[{description}]\t{message}");
        }

        public static void Debug(string message)
        {
            _log.Debug(message);
        }

        public static void Debug(string description, string message)
        {
            Debug($"[{description}]\t{message}");
        }

        public static void Error(string message)
        {
            _log.Error(message);
        }

        public static void Error(string description, string message)
        {
            Error($"[{description}]\t{message}");
        }
    }
    #endregion
}