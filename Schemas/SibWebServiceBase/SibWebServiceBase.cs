using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Terrasoft.Core;
using Terrasoft.Core.Entities;

namespace SiblionIntegration
{
    public abstract class SibWebServiceBase
    {
        private ServiceInfo _serviceInfo;
        public ServiceInfo ServiceInfo { get => _serviceInfo; protected set => _serviceInfo = value; }

        private SibWSExecutorBase _executor;

        private UserConnection _userConnection;
        protected UserConnection UserConnection
        {
            get
            {
                if (_userConnection != null)
                {
                    return _userConnection;
                }
                _userConnection = HttpContext.Current.Session["UserConnection"] as UserConnection;
                if (_userConnection != null)
                {
                    return _userConnection;
                }
                var appConnection = (AppConnection)HttpContext.Current.Application["AppConnection"];
                _userConnection = appConnection.SystemUserConnection;
                return _userConnection;
            }

            set
            {
                _userConnection = value;
            }
        }

        private string _address;
        public string Address
        {
            get
            {
                if (String.IsNullOrEmpty(_address))
                {
                    _address = GetAddress();
                }
                return _address;
            }
        }

        private string _username;
        public string UserName
        {
            get
            {
                if (String.IsNullOrEmpty(_username))
                {
                    _username = GetUserName();
                }
                return _username;
            }
        }

        private string _password;
        public string Password
        {
            get
            {
                if (String.IsNullOrEmpty(_password))
                {
                    _password = GetPassword();
                }
                return _password;
            }
        }

        private int _timeout;
        public int Timeout
        {
            get
            {
                if (_timeout == 0)
                {
                    _timeout = GetTimeout();
                }
                return _timeout;
            }
        }

        protected abstract string LogServiceName { get; }


        public SibWebServiceBase() { }

        public SibWebServiceBase(string serviceCode)
        {
            InitServiceInfo(serviceCode);
        }

        public SibWebServiceBase(UserConnection userConnection, string serviceCode)
        {
            _userConnection = userConnection;
            InitServiceInfo(serviceCode);
        }


        public void InitializeExecutor<T>() where T : SibWSExecutorBase
        {
            _executor = (T)Activator.CreateInstance(typeof(T), UserConnection, ServiceInfo);
            _executor.LogServiceName = LogServiceName;
        }

        public void AddParameter(string parameterName, object parameterValue)
        {
            if (_executor == null)
                throw new Exception("Перед добавлением параметров необходимо выполнить инициализацию Executor при помощи вызова метода 'InitializeExecutor<T>'");

            _executor.Parameters.Add(parameterName, parameterValue);
        }

        public void BindToLog(Guid logId)
        {
            if (_executor == null)
                throw new Exception("Перед выполнением привязки к существующему логу интеграции необходимо выполнить инициализацию Executor при помощи вызова метода 'InitializeExecutor<T>'");

            if (logId == Guid.Empty) return;

            _executor.LogService = SibIntegrationLogService.GetExistingLogService(logId);
            if (_executor.LogService != null) _executor.ExternalLogOption = true;
        }

        public object Execute()
        {
            if (_executor == null)
                throw new Exception("Перед запуском сервиса необходимо выполнить инициализацию Executor при помощи вызова метода 'InitializeExecutor<T>'");

            return _executor.Execute();
        }

        protected abstract string GetAddress();
        protected abstract string GetUserName();
        protected abstract string GetPassword();
        protected abstract int GetTimeout();

        protected void InitServiceInfo(string serviceCode)
        {
            ServiceInfo = GetWebServiceDescription(UserConnection, serviceCode);
            ServiceInfo.URL = Address;
            ServiceInfo.UserName = UserName;
            ServiceInfo.Password = Password;
            ServiceInfo.Timeout = Timeout;
        }

        protected T GetSystemPreference<T>(string sysPrefCode)
        {
            object sysPref;
            T result = default(T);
            Terrasoft.Core.Configuration.SysSettings.TryGetValue(_userConnection, sysPrefCode, out sysPref);
            if (sysPref != null)
            {
                return (T)sysPref;
            }
            else
            {
                throw new System.Exception(
                    String.Format("Ошибка при получении системной настройки с кодом '{0}'. Убедитесь, что данная системная настройка существует, и её значение заполнено.", 
                    sysPrefCode));
            }
        }

        public static ServiceInfo GetWebServiceDescription(UserConnection userConnection, string serviceCode)
        {
            ServiceInfo serviceInfo = null;

            var esq = new EntitySchemaQuery(userConnection.EntitySchemaManager, "SibWebServiceInfo");
            esq.AddAllSchemaColumns();
            esq.Filters.Add(
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "Code", serviceCode)
            );
            var entityCollection = esq.GetEntityCollection(userConnection);
            if (entityCollection.Count() > 0)
            {
                var entity = entityCollection[0];
                serviceInfo = new ServiceInfo();
                serviceInfo.Id = entity.PrimaryColumnValue;
                serviceInfo.Name = entity.GetTypedColumnValue<string>("Name");
                serviceInfo.Code = entity.GetTypedColumnValue<string>("Code");
                serviceInfo.Direction = SibIntegrationConstants.WSDirectionNames.First(x => x.Value == entity.GetTypedColumnValue<string>("Direction")).Key;
            }
            else
            {
                throw new Exception(String.Format("Не найдено описание веб-сервиса '{0}' в справочнике 'Описание веб-сервисов'.", serviceCode));
            }
            return serviceInfo;
        }
    }
}