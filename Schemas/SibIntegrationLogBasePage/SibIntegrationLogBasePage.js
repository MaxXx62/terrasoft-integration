define("SibIntegrationLogBasePage", ["SibIntegrationLogBasePageResources", "BusinessRuleModule"],
function(Resources, BusinessRuleModule) {
    return {
        entitySchemaName: "SibIntegrationLogBase",
        details: /* SCHEMA_DETAILS */{}/* SCHEMA_DETAILS */,
        messages: {
            "RefreshEntity": {
                mode: Terrasoft.MessageMode.PTP,
                direction: Terrasoft.MessageDirectionType.SUBSCRIBE
            }
        },
        attributes: {
            "SibIntegrationStatus": {
                "lookupListConfig": {
                    columns: ["Code"]
                }
            },
            "CountRecordsComplete": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.INTEGER,
                value: 0
            },
            "CountRecordsError": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.INTEGER,
                value: 0
            },
            "CountRecordsWarning": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.INTEGER,
                value: 0
            },
            "InboundMessageLink": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            },
            "OutboundMessageLink": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            },
            "CreatedOnExt": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            },
            "EndedOnExt": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            },
            "ExternalStartedOnExt": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            },
            "ExternalEndedOnExt": {
                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                dataValueType: Terrasoft.DataValueType.TEXT,
                value: ""
            }
        },
        diff: /* SCHEMA_DIFF */[
            {
                "operation": "insert",
                "name": "Name",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 0, "colSpan": 12, "rowSpan": 1},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "Direction",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 1, "colSpan": 12, "rowSpan": 1},
                    "enabled": false,
                }
            },
            {
                "operation": "insert",
                "name": "Id",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 2, "colSpan": 12, "rowSpan": 1},
                    "dataValueType": Terrasoft.DataValueType.TEXT,
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "SibIntegrationStatus",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 3, "colSpan": 12, "rowSpan": 1},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "CreatedBy",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 12, "row": 0, "colSpan": 12, "rowSpan": 1},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "CreatedOnExt",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 12, "row": 1, "colSpan": 6},
                    "caption": {"bindTo": "Resources.Strings.CreatedOnCaption"},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "EndedOnExt",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 12, "row": 2, "colSpan": 6},
                    "caption": {"bindTo": "Resources.Strings.EndedOnCaption"},
                    "enabled": false
                },
            },
            {
                "operation": "insert",
                "name": "ExternalStartedOnExt",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 18, "row": 1, "colSpan": 6},
                    "caption": {"bindTo": "Resources.Strings.ExternalStartedOnCaption"},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "ExternalEndedOnExt",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 18, "row": 2, "colSpan": 6},
                    "caption": {"bindTo": "Resources.Strings.ExternalEndedOnCaption"},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "RecordCount",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 12, "row": 3, "colSpan": 12, "rowSpan": 1},
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "CountRecordsComplete",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 4, "colSpan": 12, "rowSpan": 1},
                    "caption": {"bindTo": "Resources.Strings.CountRecordsCompleteCaption"},
                    "bindTo": "CountRecordsComplete",
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "CountRecordsError",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 12, "row": 4, "colSpan": 6, "rowSpan": 1},
                    "caption": {"bindTo": "Resources.Strings.CountRecordsErrorCaption"},
                    "bindTo": "CountRecordsError",
                    "enabled": false
                }
            },
            {
                "operation": "insert",
                "name": "CountRecordsWarning",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 18, "row": 4, "colSpan": 6, "rowSpan": 1},
                    "caption": {"bindTo": "Resources.Strings.CountRecordsWarningCaption"},
                    "bindTo": "CountRecordsWarning",
                    "enabled": false
                }
            },
            {
            	"operation": "insert",
            	"name": "OutboundMessageLink",
                "parentName": "Header",
            	"propertyName": "items",
            	"values": {
                    "caption": {"bindTo": "Resources.Strings.OutboundMessageCaption"},
            		"showValueAsLink": true,
            		"controlConfig": {
            			"enabled": false,
            			"href": {
            				"bindTo": "getOutboundMessageLink"
            			},
            			"linkclick": {
            				"bindTo": "onOutboundMessageLinkClick"
            			}
            		},
            		"layout": {
            			"column": 0, "row": 5, "colSpan": 12, "rowSpan": 1,
            			"layoutName": "Header"
            		},
            		"labelConfig": {},
            		"enabled": false,
            		"bindTo": "OutboundMessageLink"
            	}
            },
            {
            	"operation": "insert",
            	"name": "InboundMessageLink",
                "parentName": "Header",
            	"propertyName": "items",
            	"values": {
                    "caption": {"bindTo": "Resources.Strings.InboundMessageCaption"},
            		"showValueAsLink": true,
            		"controlConfig": {
            			"enabled": false,
            			"href": {
            				"bindTo": "getInboundMessageLink"
            			},
            			"linkclick": {
            				"bindTo": "onInboundMessageLinkClick"
            			}
            		},
            		"layout": {
            			"column": 12, "row": 5, "colSpan": 12, "rowSpan": 1,
            			"layoutName": "Header"
            		},
            		"labelConfig": {},
            		"enabled": false,
            		"bindTo": "InboundMessageLink"
            	}
            },
            {
                "operation": "insert",
                "name": "ErrorText",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 7, "colSpan": 24, "rowSpan": 3},
                    "enabled": false,
                    "controlConfig": {
                        "height": "96px"
                    },
                    "contentType": Terrasoft.ContentType.LONG_TEXT
                },
            },
            {
                "operation": "insert",
                "name": "GeneralInfoTab",
                "values": {
                    "caption": {"bindTo": "Resources.Strings.GeneralInfoCaption"},
                    "items": []
                },
                "parentName": "Tabs",
                "propertyName": "tabs",
                "index": 0
            },
            {
                "operation": "insert",
                "parentName": "GeneralInfoTab",
                "name": "GeneralInfoContainer",
                "propertyName": "items",
                "values": {
                    itemType: Terrasoft.ViewItemType.CONTAINER,
                    "items": []
                }
            },
            {
                "operation": "insert",
                "name": "SibIntegrationLogItemDetail",
                "values": {
                    "itemType": 2,
                    "markerValue": "added-detail"
                },
                "parentName": "GeneralInfoTab",
                "propertyName": "items",
                "index": 1
            }
        ],/* SCHEMA_DIFF */
        methods: {
            /*
            * overriden
            */
            onEntityInitialized: function () {
                this.callParent(arguments);
                this.getMessageLinks();
                this.getProcessedRecordCounts();
                this.getProcessedWarningRecordCounts();
                this.updateInfo();
                this.updateExtFields();
            },

            /*
            * overriden
            */
            onDestroy: function() {
                this.callParent(arguments);
                clearInterval(this.SibInterval);
            },

            getMessageLinks: function() {
                var recordId = this.get("Id");
                if (!recordId) {
                    return "";
                }

                var inboundLink;
                if (this.get("MessagesStorageMethod") == 0) {
                    inboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFile/" +
                        this.entitySchemaName + "/InboundMessage/in/" + recordId;
                } else {
                    inboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFileFromFileSystem/" +
                        this.entitySchemaName + "/InboundMessage/in/" + recordId;
                }
                this.set("InboundMessageLink", inboundLink);

                var outboundLink;
                if (this.get("MessagesStorageMethod") == 0) {
                    outboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFile/" +
                        this.entitySchemaName + "/OutboundMessage/out/" + recordId;
                } else {
                    outboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFileFromFileSystem/" +
                        this.entitySchemaName + "/OutboundMessage/out/" + recordId;
                }
                this.set("OutboundMessageLink", outboundLink);
            },

            getProcessedRecordCounts: function() {
                var logId = this.get("Id");

                var completeStatus = '7f8338b5-c476-4389-9cf8-8df99edf9d4d';
                esq = this.getProcessedRecordCountESQ(logId, completeStatus);
                esq.getEntityCollection(function(response) {
                    if (response && response.success && response.collection.getCount() > 0) {
                        this.set("CountRecordsComplete", response.collection.getByIndex(0).get("Count"));
                    }
                }, this);

                var errorStatus = 'cc312205-1ef6-4a2e-a2a5-f2fb2cd25382';
                esq = this.getProcessedRecordCountESQ(logId, errorStatus);
                esq.getEntityCollection(function(response) {
                    if (response && response.success && response.collection.getCount() > 0) {
                        this.set("CountRecordsError", response.collection.getByIndex(0).get("Count"));
                    }
                }, this);
            },

            getProcessedWarningRecordCounts: function() {
                var logId = this.get("Id");

                var completeStatus = '7f8338b5-c476-4389-9cf8-8df99edf9d4d';
                esq = this.getProcessedRecordCountESQ(logId, completeStatus);
                this.addErrorTextFilterForProcessedRecordCountsESQ(esq);
                esq.getEntityCollection(function(response) {
                    if (response && response.success && response.collection.getCount() > 0) {
                        this.set("CountRecordsWarning", response.collection.getByIndex(0).get("Count"));
                    }
                }, this);
            },

            getProcessedRecordCountESQ: function(logId, statusId) {
                esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
                    rootSchemaName: this.entitySchemaName + "Item"
                });
                esq.addAggregationSchemaColumn("Id", this.Terrasoft.AggregationType.COUNT, "Count");
                esq.filters.add("LogFilter", this.Terrasoft.createColumnFilterWithParameter(
                    this.Terrasoft.ComparisonType.EQUAL, "SibIntegrationLog.Id", logId));
                esq.filters.add("StatusFilter", this.Terrasoft.createColumnFilterWithParameter(
                    this.Terrasoft.ComparisonType.EQUAL, "SibIntegrationStatus.Id", statusId));
                return esq;
            },

            addErrorTextFilterForProcessedRecordCountsESQ: function(esq) {
                if (esq) {
                    esq.filters.add("ErrorTextFilter",
                        this.Terrasoft.createColumnIsNotNullFilter("ErrorText"));
                }
            },

            updateInfo: function() {
                clearInterval(this.SibInterval);
                if (this.get("SibIntegrationStatus").Code != 1) return;

                var scope = this;
                this.SibInterval = setInterval(function () {
                    scope.getCurrentStatus(scope.get("Id"));
                    if (scope.get("SibIntegrationStatus").Code != 1) {
                        clearInterval(scope.SibInterval);
                    }
                    scope.getProcessedRecordCounts();
                }, 5000);
            },

            getCurrentStatus: function(logId) {
                esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
                    rootSchemaName: this.entitySchemaName
                });
                esq.addColumn("SibIntegrationStatus");
                esq.addColumn("SibIntegrationStatus.Code", "Code");
                esq.addColumn("EndedOn");
                esq.addColumn("ErrorText");
                esq.addColumn("RecordCount");
                esq.getEntity(logId, function(result) {
                    if (result.success) {
                        this.set("SibIntegrationStatus", {
                            value: result.entity.get("SibIntegrationStatus").value,
                            displayValue: result.entity.get("SibIntegrationStatus").displayValue,
                            Code: result.entity.get("Code")
                        });
                        this.set("RecordCount", result.entity.get("RecordCount"));
                        this.set("EndedOn", result.entity.get("EndedOn"));
                        this.set("ErrorText", result.entity.get("ErrorText"));
                    }
                }, this);
            },

            getInboundMessageLink: function() {
        		return this.getLink(this.get("InboundMessageLink"), this.get("Resources.Strings.InboundMessageCaption"));
        	},

            getOutboundMessageLink: function() {
                return this.getLink(this.get("OutboundMessageLink"), this.get("Resources.Strings.OutboundMessageCaption"));
            },

        	onInboundMessageLinkClick: this.Terrasoft.emptyFn,
            onOutboundMessageLinkClick: this.Terrasoft.emptyFn,

        	getLink: function(link, caption) {
        		if (Terrasoft.isUrl(link)) {
        			return {
        				url: link,
        				caption: caption
        			};
        		}
        	},

            updateExtFields: function() {
                this.set("CreatedOnExt", this.getFullDateTimeString(this.get("CreatedOn")));
                this.set("EndedOnExt", this.getFullDateTimeString(this.get("EndedOn")));
                this.set("ExternalStartedOnExt", this.getFullDateTimeString(this.get("ExternalStartedOn")));
                this.set("ExternalEndedOnExt", this.getFullDateTimeString(this.get("ExternalEndedOn")));
            },

            getFullDateTimeString: function(dateTimeValue) {
                if (!dateTimeValue) return null;
                return dateTimeValue.toLocaleDateString() + '    ' + dateTimeValue.toLocaleTimeString()  + '.' +
                    dateTimeValue.getMilliseconds();
            }
        },
        rules: {
            "CountRecordsComplete": {
                "CountRecordsCompleteVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "LogLevel"
                        },
                        "comparisonType": Terrasoft.ComparisonType.GREATER,
                        "rightExpression": {
                            "type": BusinessRuleModule.enums.ValueType.CONSTANT,
                            "value": "1"
                        },
                    }]
                }
            },
            "ExternalStartedOnExt": {
                "ExternalStartedOnExtVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "ExternalStartedOnExt"
                        },
                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
                    }]
                }
            },
            "ExternalEndedOnExt": {
                "ExternalEndedOnExtVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "ExternalEndedOnExt"
                        },
                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
                    }]
                }
            },
            "ErrorText": {
                "ErrorTextVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "ErrorText"
                        },
                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
                    }]
                }
            },
            "OutboundMessageLink": {
                "OutboundMessageLinkVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "OutboundMessage"
                        },
                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
                    }]
                }
            },
            "InboundMessageLink": {
                "InboundMessageLinkVisibile": {
                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    "property": BusinessRuleModule.enums.Property.VISIBLE,
                    "conditions": [{
                        "leftExpression": {
                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            "attribute": "InboundMessage"
                        },
                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
                    }]
                }
            }
        }
    };
});
