define("SibIntegrationQueueBasePage", ["SibIntegrationQueueBasePageResources", "BusinessRuleModule"],
	function(Resources, BusinessRuleModule) {
	    return {
	        entitySchemaName: "SibIntegrationQueueBase",
	        messages: {},
	        attributes: {
				"CreatedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
	            "SibModifiedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
                "EndedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
                "LastTryOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
			},
	        diff: /* SCHEMA_DIFF */[
                {
                    "operation": "insert",
                    "parentName": "LeftContainer",
                    "propertyName": "items",
                    "name": "IncreasePriorityButton",
                    "values": {
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: {bindTo: "Resources.Strings.IncreasePriorityButtonCaption"},
                        click: {bindTo: "onIncreasePriorityButtonClick"},
                        enabled: {bindTo: "isIncreasePriorityButtonEnabled"},
                        style: Terrasoft.controls.ButtonEnums.style.GREEN
                    }
                },
                //Тип записи
                /**
                {
	                "operation": "insert",
	                "name": "RecordType",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 0, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
                //Id записи
                {
	                "operation": "insert",
	                "name": "RecordId",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 1, "colSpan": 12, "rowSpan": 1},
	                    "dataValueType": Terrasoft.DataValueType.TEXT,
	                    "enabled": false
	                }
	            },
                /**/
				{
	                "operation": "insert",
	                "name": "Id",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 2, "colSpan": 12, "rowSpan": 1},
	                    "dataValueType": Terrasoft.DataValueType.TEXT,
	                    "enabled": false
	                }
	            },
                {
	                "operation": "insert",
	                "name": "SibIntegrationStatus",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 3, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "Priority",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 4, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "CreatedBy",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 0, "colSpan": 6, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
                {
	                "operation": "insert",
	                "name": "SibModifiedBy",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 18, "row": 0, "colSpan": 6, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "CreatedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 1, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.CreatedOnCaption"},
	                    "enabled": false
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "SibModifiedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 18, "row": 1, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.ModifiedOnCaption"},
	                    "enabled": false
	                },
	            },
                {
	                "operation": "insert",
	                "name": "EndedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 2, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.EndedOnCaption"},
	                    "enabled": false
	                },
	            },
                {
	                "operation": "insert",
	                "name": "ErrorCount",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 3, "colSpan": 6},
	                    "enabled": false
	                },
	            },
                {
	                "operation": "insert",
	                "name": "LastTryOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 18, "row": 3, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.LastTryOnCaption"},
	                    "enabled": false
	                },
	            },
                {
	                "operation": "insert",
	                "name": "IsError",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 4, "colSpan": 6},
	                    "enabled": false
	                },
	            }
			],/* SCHEMA_DIFF */
	        methods: {
                /*
	            * overriden
	            */
                getPageHeaderCaption: function() {
					return this.get("Resources.Strings.PageHeaderCaption");
				},

				/*
	            * overriden
	            */
	            onEntityInitialized: function () {
	                this.callParent(arguments);
	                this.updateExtFields();
	            },

	            updateExtFields: function() {
	                this.set("CreatedOnExt", this.getFullDateTimeString(this.get("CreatedOn")));
	                this.set("SibModifiedOnExt", this.getFullDateTimeString(this.get("SibModifiedOn")));
	                this.set("EndedOnExt", this.getFullDateTimeString(this.get("EndedOn")));
                    this.set("LastTryOnExt", this.getFullDateTimeString(this.get("LastTryOn")));
	            },

                onIncreasePriorityButtonClick: function() {
                    this.set("IsInChain", false);
                },

                isIncreasePriorityButtonEnabled: function() {
                    var integrationStatus__Complete = '7f8338b5-c476-4389-9cf8-8df99edf9d4d';
                    var status = this.get("SibIntegrationStatus");
                    return (!status) || status.value != integrationStatus__Complete;
                },

	            getFullDateTimeString: function(dateTimeValue) {
	                if (!dateTimeValue) return null;
	                return dateTimeValue.toLocaleDateString() + '    ' + dateTimeValue.toLocaleTimeString()  + '.' +
	                    dateTimeValue.getMilliseconds();
	            }
			},
	        rules: {
				"EndedOnExt": {
	                "EndedOnExtVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "EndedOnExt"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "LastTryOnExt": {
	                "LastTryOnExtVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "LastTryOnExt"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "ErrorText": {
	                "ErrorTextVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "ErrorText"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            }
			}
	    };
	});
