define("SibIntegrationLogBaseSection", ["ConfigurationEnums", "ModalBox", "GridUtilitiesV2"],
    function(ConfigurationEnums, ModalBox) {
        return {
            entitySchemaName: "SibIntegrationLogBase",
            messages: {
                "RecordIdFromModal" : {
    				mode: Terrasoft.MessageMode.PTP,
    				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
                }
            },
            attributes: {
                "UseSectionHeaderCaption": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					value: true
				},
                "IsCardHide": {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    dataValueType: Terrasoft.DataValueType.BOOLEAN,
                    value: true
                },
                "IsRecordSearch": {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    dataValueType: Terrasoft.DataValueType.BOOLEAN,
                    value: false
                },
                "SearchRecordPrimaryId": {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    value: ""
                },
                "SearchRecordSecondaryId": {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    dataValueType: Terrasoft.DataValueType.TEXT,
                    value: ""
                }
            },
            mixins: {},
            methods: {
                /*
				* overriden
				*/
                init: function() {
                    this.callParent(arguments);
                    this.set("IsIncludeInFolderButtonVisible", false);
                    this.subscribeSandboxEventsForSearchRecord();
                },

                /*
                * overriden
                */
                initFixedFiltersConfig: function() {
                    var fixedFilterConfig = {
                        entitySchema: this.entitySchema,
                        filters: [
                            {
                                name: "PeriodFilter",
                                caption: this.get("Resources.Strings.PeriodFilterCaption"),
                                dataValueType: this.Terrasoft.DataValueType.DATE,
                                startDate: {
                                    columnName: "CreatedOn",
                                    defValue: this.Terrasoft.startOfWeek(new Date())
                                },
                                dueDate: {
                                    columnName: "CreatedOn",
                                    defValue: this.Terrasoft.endOfWeek(new Date())
                                }
                            }
                        ]
                    };
                    this.set("FixedFilterConfig", fixedFilterConfig);
                },

                /*
                * overriden
                */
                initQueryFilters: function(esq) {
    				this.callParent(arguments);
                    if (this.get("IsRecordSearch")) {
                        this.set("IsRecordSearch", false);
                        esq.filters.addItem(this.getItemRecordIdFilter(esq));
                    }
    			},

                /*
                * overriden
                */
                showCard: function() {
                    this.callParent(arguments);
                    this.set("IsCardHide", false);
                },

                /*
                * overriden
                */
                hideCard: function() {
                    this.callParent(arguments);
                    this.set("IsCardHide", true);
                },

                /*
                * overriden
                */
                onCardVisibleChange: function() {
                    this.set("IsCardHide", !this.get("IsCardVisible"));
                },

                /*
				* overriden
				*/
                addSectionDesignerViewOptions: this.Terrasoft.emptyFn,
                addSectionHistoryState: this.Terrasoft.emptyFn,
                removeSectionHistoryState: this.Terrasoft.emptyFn,
                initRunProcessButtonMenu: this.Terrasoft.emptyFn,

                /*
                * overriden
                */
                addCardHistoryState: function(schemaName, operation, primaryColumnValue) {
                    if (!schemaName) {
                        return;
                    }
                    var cardOperationConfig = {
                        schemaName: schemaName,
                        operation: operation,
                        primaryColumnValue: primaryColumnValue
                    };
                    var historyState = this.getHistoryStateInfo();
                    var stateConfig = this.getCardHistoryStateConfig(cardOperationConfig);
                    var eventName = (historyState.workAreaMode === ConfigurationEnums.WorkAreaMode.COMBINED)
                        ? "ReplaceHistoryState"
                        : "PushHistoryState";
                    this.sandbox.publish(eventName, stateConfig);
                },

                /*
                * overriden
                */
                removeCardHistoryState: function () {
                    var module = "SectionModuleV2";
                    var schema = this.name;
                    var historyState = this.sandbox.publish("GetHistoryState");
                    var currentState = historyState.state;
                    var newState = {
                        moduleId: currentState.moduleId
                    };
                    var hash = [module, schema].join("/");
                    this.sandbox.publish("PushHistoryState", {
                        hash: hash,
                        stateObj: newState,
                        silent: true
                    });
                },

                /*
                * overriden
                */
                getDefaultDataViews: function() {
                    var dataViews = this.callParent(arguments);
                    delete dataViews.AnalyticsDataView;
                    return dataViews;
                },

                closeSection: function() {
                    var module = "SystemDesigner";
                    this.sandbox.publish("PushHistoryState", {
                        hash: this.Terrasoft.combinePath("IntroPage", module)
                    });
                },

                subscribeSandboxEventsForSearchRecord: function() {
        			this.sandbox.subscribe("RecordIdFromModal", function(params) {
                        if (params.recordPrimaryId) {
                            this.set("IsRecordSearch", true);
            				this.set("SearchRecordPrimaryId", params.recordPrimaryId.toLowerCase());
                            this.set("SearchRecordSecondaryId", params.recordSecondaryId && params.recordSecondaryId.toLowerCase());
                            this.reloadGridData();
                        }
        			}, this, [this.getSearchRecordModuleId()]);
        		},

                onSearchRecordButtonSectionClick: function() {
                    this.showSearchRecordModalBox();
                },

                showSearchRecordModalBox: function() {
        			var sandbox = this.sandbox;
        			var config = {
        				widthPixels: 400,
        				heightPixels: 200
        			};
        			var moduleId = this.getSearchRecordModuleId();
        			var renderTo = ModalBox.show(config, function() {
        				sandbox.unloadModule(moduleId, renderTo);
        			}, this);
        			sandbox.loadModule(this.getSearchRecordModuleName(), {
        				id: moduleId,
        				renderTo: renderTo
        			});
        		},

                getSearchRecordModuleName: function() {
        			return "SibIntegrationLogSearchRecordModule";
        		},

        		getSearchRecordModuleId: function() {
        			return this.sandbox.id + "_module_" + this.getSearchRecordModuleName();
        		},

                getItemRecordIdFilter: function(esq) {
                    var entitySchemaName = this.entitySchemaName;
                    var filter = Ext.String.format("[{0}Item:SibIntegrationLog:Id].Id", entitySchemaName);

                    var recordPrimaryId = this.get("SearchRecordPrimaryId");
                    var recordSecondaryId = this.get("SearchRecordSecondaryId");
                    var existsFilter = esq.createExistsFilter(filter);
                    existsFilter.subFilters.addItem(
                        Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "RecordId", recordPrimaryId)
                    );
                    if (recordSecondaryId) {
                        existsFilter.subFilters.logicalOperation = Terrasoft.LogicalOperatorType.OR;
                        existsFilter.subFilters.addItem(
                            Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "RecordId", recordSecondaryId)
                        );
                    }
                    return existsFilter;
                }
            },
            diff: /**SCHEMA_DIFF*/[
                {
                    "operation": "merge",
                    "name": "SeparateModeAddRecordButton",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "DataGridActiveRowCopyAction",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "DataGridActiveRowDeleteAction",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "merge",
                    "name": "CombinedModeAddRecordButton",
                    "values": {
                        "visible": false
                    }
                },
                {
                    "operation": "insert",
                    "name": "SeparateModeBackButton",
                    "parentName": "SeparateModeActionButtonsLeftContainer",
                    "propertyName": "items",
                    "index": 2,
                    "values": {
                        "itemType": this.Terrasoft.ViewItemType.BUTTON,
                        "caption": {"bindTo": "Resources.Strings.BackButtonCaption"},
                        "click": {"bindTo": "closeSection"},
                        "classes": {
                            "textClass": ["actions-button-margin-right"],
                            "wrapperClass": ["actions-button-margin-right"]
                        },
                        "visible": {"bindTo": "SeparateModeActionsButtonVisible"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ActionButtonsContainer",
                    "propertyName": "items",
                    "name": "RefreshButtonSection",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "style": Terrasoft.controls.ButtonEnums.style.GREEN,
                        "caption": {"bindTo": "Resources.Strings.RefreshButtonCaption"},
                        "click": {"bindTo": "onRefreshButtonSectionClick"},
                        "visible": {"bindTo": "IsCardHide"}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "CombinedModeActionButtonsCardLeftContainer",
                    "propertyName": "items",
                    "name": "RefreshButtonCombined",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "style": Terrasoft.controls.ButtonEnums.style.GREEN,
                        "caption": {"bindTo": "Resources.Strings.RefreshButtonCaption"},
                        "click": {"bindTo": "onRefreshButtonCombinedClick"},
                        "visible": true
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "ActionButtonsContainer",
                    "propertyName": "items",
                    "name": "SearchRecordButton",
                    "values": {
                        "itemType": Terrasoft.ViewItemType.BUTTON,
                        "style": Terrasoft.controls.ButtonEnums.style.BLUE,
                        "caption": {"bindTo": "Resources.Strings.SearchRecordButtonCaption"},
                        "click": {"bindTo": "onSearchRecordButtonSectionClick"},
                        "visible": {"bindTo": "IsCardHide"},
                        "hint": {"bindTo": "Resources.Strings.SearchButtonHintCaption"}
                    }
                },
            ]/**SCHEMA_DIFF*/
        };
    });
