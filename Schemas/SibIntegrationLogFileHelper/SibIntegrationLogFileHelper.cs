using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.IO;

namespace SiblionIntegration
{
    [ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class SibIntegrationLogFileHelper
    {

		#region Methods: Private

		private string RemoveSpecialCharacters(string fileName) 
        {
			return Regex.Replace(fileName, @"[^a-zA-Z\p{IsCyrillic}0-9_.,^&@£$€!½§~'=()\[\]{} «»<>~#*%+-]+",
				"_", RegexOptions.Compiled);
		}

		#endregion

		#region Methods: Protected

		protected virtual void SetOutgoingResponseContentType() 
        {
			WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream";
		}

		protected virtual void SetOutgoingResponseContentLength(int size) 
        {
			WebOperationContext.Current.OutgoingResponse.ContentLength = size;
		}

		protected string GetResponseContentDisposition(string fileName) 
        {
			string contentDisposition;
			HttpRequest request = HttpContext.Current.Request;
			if (request.Browser.Browser == "IE" && (request.Browser.Version == "7.0" || request.Browser.Version == "8.0")) {
				contentDisposition = HttpUtility.UrlEncode(fileName);
			} else if (request.UserAgent != null && request.UserAgent.ToLowerInvariant().Contains("android")) {
				contentDisposition = "\"" + RemoveSpecialCharacters(fileName) + "\"";
			} else if (request.Browser.Browser == "Safari") {
				contentDisposition = RemoveSpecialCharacters(fileName);
			} else {
				contentDisposition = "\"" + fileName + "\"; filename*=UTF-8''" + HttpUtility.UrlEncode(fileName);
			}
			return String.Format("attachment; filename={0}", contentDisposition);
		}

        protected static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

		#endregion

		#region Methods: Public

		[OperationContract]
		[WebGet(UriTemplate = "GetFile/{entityName}/{columnName}/{type}/{fileId}")]
        public void GetFile(string entityName, string columnName, string type, string fileId)
        {
			SetOutgoingResponseContentType();
			var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
			string fileName = string.Empty;
            switch (type)
            {
                case "out":
                    {
                        fileName = "OutMessage_" + fileId + ".xml";
                        break;
                    }
                case "in":
                    {
                        fileName = "InMessage_" + fileId + ".xml";
                        break;
                    }
                default:
                    throw new InvalidDataException("Передан неверный параметр 'type'");
            }
			Select selectData = (new Select(userConnection)
                .Column(columnName)
                .From(entityName)
				.Where("Id")
					.IsEqual(Column.Parameter(new Guid(fileId)))) as Select;
			using (DBExecutor executor = userConnection.EnsureDBConnection()) 
            {
				using (IDataReader reader = selectData.ExecuteReader(executor, CommandBehavior.SequentialAccess)) 
                {
                    string fileData;
					while (reader.Read()) {

                        fileData = reader[columnName].ToString();
                        using (Stream s = GenerateStreamFromString(fileData))
                        {
                            SetOutgoingResponseContentLength(Convert.ToInt32(s.Length));
						    string contentDisposition = GetResponseContentDisposition(fileName);
						    HttpContext.Current.Response.AddHeader("Content-Disposition", contentDisposition);
						    HttpContext.Current.Response.ContentType = "application/octet-stream";

                            HttpContext.Current.Response.OutputStream.Write(s.ReadAllBytes(), 0, Convert.ToInt32(s.Length));
                        }
					}
				}
			}
		}

        [OperationContract]
        [WebGet(UriTemplate = "GetFileFromFileSystem/{entityName}/{columnName}/{type}/{fileId}")]
        public void GetFileFromFileSystem(string entityName, string columnName, string type, string fileId)
        {
            SetOutgoingResponseContentType();
            var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
            string fileName = string.Empty;
            switch (type)
            {
                case "out":
                {
                    fileName = "OutMessage_" + fileId + ".xml";
                    break;
                }
                case "in":
                {
                    fileName = "InMessage_" + fileId + ".xml";
                    break;
                }
                default:
                    throw new InvalidDataException("Передан неверный параметр 'type'");
            }
            Select selectData = (new Select(userConnection)
                .Column(columnName)
                .From(entityName)
                .Where("Id")
                .IsEqual(Column.Parameter(new Guid(fileId)))) as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection()) 
            {
                using (IDataReader reader = selectData.ExecuteReader(executor, CommandBehavior.SequentialAccess)) 
                {
                    string fullFileName;
                    string fileData;
                    while (reader.Read()) {

                        fullFileName = reader[columnName].ToString();
                        if (!File.Exists(fullFileName)) throw new FileNotFoundException($"Файл {fullFileName} не найден.");

                        fileData =  File.ReadAllText(fullFileName);
                        using (Stream s = GenerateStreamFromString(fileData))
                        {
                            SetOutgoingResponseContentLength(Convert.ToInt32(s.Length));
                            string contentDisposition = GetResponseContentDisposition(fileName);
                            HttpContext.Current.Response.AddHeader("Content-Disposition", contentDisposition);
                            HttpContext.Current.Response.ContentType = "application/octet-stream";

                            HttpContext.Current.Response.OutputStream.Write(s.ReadAllBytes(), 0, Convert.ToInt32(s.Length));
                        }
                    }
                }
            }
		}

		#endregion

	}
}