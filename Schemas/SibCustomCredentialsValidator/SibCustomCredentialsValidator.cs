using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.Entities;

namespace SiblionIntegration
{
    public class SibCustomCredentialsValidator
    {
        private readonly FaultException _invalidAccessException = new FaultException("Access denied: username or password is invalid");

        private UserConnection _userConnection;
        private List<ServiceUser> _users;
        private Dictionary<Func<object, ServiceUser>, object> _validateMethods;

        public SibCustomCredentialsValidator(UserConnection userConnection, Guid serviceInfoId)
        {
            _userConnection = userConnection;
            _validateMethods = new Dictionary<Func<object, ServiceUser>, object>();
            _users = GetServiceUsers(serviceInfoId);
        }

        public SibCustomCredentialsValidator WithOption(ValidateOption option, object parameter = null)
        {
            switch (option)
            {
                case ValidateOption.Forms:
                {
                    _validateMethods.Add(ValidateCookiesAuth, parameter);
                    break;
                }
                case ValidateOption.HttpHeader:
                {
                    _validateMethods.Add(ValidateHttpHeaderAuth, parameter);
                    break;
                }
                case ValidateOption.SoapHeader:
                {
                    _validateMethods.Add(ValidateSoapHeaderAuth, parameter);
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return this;
        }

        public ServiceUser Validate()
        {
            foreach (var validateItem in _validateMethods)
            {
                var user = validateItem.Key(validateItem.Value);
                if (user != null) return user; 
            }

            throw _invalidAccessException;
        }

        private List<ServiceUser> GetServiceUsers(Guid serviceInfoId)
        {
            var users = new List<ServiceUser>();
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, "SibWebServiceUsers");
            esq.AddAllSchemaColumns();
            esq.Filters.Add(
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "SibWebServiceInfo", serviceInfoId)
            );
            var entityCollection = esq.GetEntityCollection(_userConnection);
            if (entityCollection.Count == 0) throw _invalidAccessException;
            foreach(var entity in entityCollection)
            {
                var user = new ServiceUser();
                user.Username = entity.GetTypedColumnValue<string>("Name");
                user.Password = entity.GetTypedColumnValue<string>("Password");
                user.Description = entity.GetTypedColumnValue<string>("Description");
                users.Add(user);
            }
            return users;
        }

        #region Validators
        private ServiceUser ValidateCookiesAuth(object parameter = null)
        {
            return _users.FirstOrDefault(x => x.Username == _userConnection.CurrentUser.Name);
        }

        private ServiceUser ValidateHttpHeaderAuth(object parameter = null)
        {
            if (WebOperationContext.Current == null) return null;

            var userName = WebOperationContext.Current.IncomingRequest.Headers["Username"];
            var password = WebOperationContext.Current.IncomingRequest.Headers["Password"];

            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(password))
                return null;
            else
                return _users.FirstOrDefault(x => x.Username == userName && x.Password == password);
        }

        private ServiceUser ValidateSoapHeaderAuth(object parameter)
        {
            if (OperationContext.Current == null) return null;

            var nmspace = (string)parameter;

            int index = OperationContext.Current.IncomingMessageHeaders.FindHeader("Username", nmspace);
            var userName = (index >= 0) ? OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(index) : null;

            index = OperationContext.Current.IncomingMessageHeaders.FindHeader("Password", nmspace);
            var password = (index >= 0) ? OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(index) : null;

            if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(password))
                return null;
            else
                return _users.FirstOrDefault(x => x.Username == userName && x.Password == password);
        }
        #endregion
    }

    public enum ValidateOption
    {
        Forms,
        HttpHeader,
        SoapHeader
    }
}