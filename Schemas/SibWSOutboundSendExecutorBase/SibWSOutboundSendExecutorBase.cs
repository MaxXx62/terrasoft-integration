using System;
using Terrasoft.Common;
using Terrasoft.Core;

namespace SiblionIntegration
{
    public abstract class SibWSOutboundSendExecutorBase: SibWSOutboundExecutorBase
    {
        protected Guid RecordId { get; set; }
        protected string RecordIdString { get; set; }
        protected string RecordType { get; set; }

        public SibWSOutboundSendExecutorBase(UserConnection userConnection, ServiceInfo serviceInfo) : 
            base(userConnection, serviceInfo)
        {
        }

        public override object Execute()
        {
            OptionalOverrideServiceInfo();

            InitRecordId();

            CreatePrimaryLogRecord();
            CreateChildLogRecord(RecordIdString, RecordType, ServiceInfo.DirectionName);

            object serviceParameter = null, serviceResponse = null, processDataResponse = null;
            try
            {
                try
                {
                    serviceParameter = PrepareData();
                }
                catch(WSBusinessException e)
                {
                    throw;
                }
                catch(Exception e)
                {
                    throw new WSOutboundPrepareDataException("Ошибка формирования входных данных исходящего веб-сервиса.", e);
                }
                SaveOutboundMessageToPrimaryLog(serviceParameter);
                SaveOutboundMessageToChildLog(serviceParameter);

                try
                {
                    SaveExternalStartTimeToLog();
                    serviceResponse = CallWebService(serviceParameter);
                }
                catch (Exception e)
                {
                    throw new WSOutboundCallWebServiceException("Ошибка вызова исходящего веб-сервиса.", e);
                }
                finally
                {
                    SaveExternalEndTimeToLog();
                }
                SaveInboundMessageToPrimaryLog(serviceResponse);
                SaveInboundMessageToChildLog(serviceResponse);

                try
                {
                    processDataResponse = ProcessData(serviceResponse);
                }
                catch (WSExpectedException e)
                {
                    throw;
                }
                catch(WSBusinessException e)
                {
                    throw;
                }
                catch (WSExternalSystemException e)
                {
                    throw;
                }
                catch (Exception e)
                {
                    throw new WSOutboundProcessDataException("Ошибка обработки данных, полученных от веб-сервиса.", e);
                }
            }
            catch (WSExpectedException e)
            {
                ErrorException = e;
            }
            catch (WSBusinessException e)
            {
                ErrorException = e;
                throw;
            }
            catch (Exception e)
            {
                ErrorException = e;
                throw;
            }
            finally
            {
                CloseChildLogRecord();
                ClosePrimaryLogRecord();
            }

            return new ServiceResult {
                IsSuccess = (ErrorException == null || ErrorException.GetType() == typeof(WSExpectedException)),
                Result = processDataResponse
            };
        }

        private void InitRecordId()
        {
            if (Parameters.ContainsKey("RecordId"))
            {
                var recordIdParam = Parameters["RecordId"];
                if (recordIdParam == null) return;

                if (recordIdParam is Guid)
                {
                    RecordId = (Guid)recordIdParam;
                    RecordIdString = RecordId.ToString();
                    if (RecordId == Guid.Empty)
                        throw new Exception($"Параметр 'RecordId' не может быть равным '{RecordId}'.");
                }
                else if (recordIdParam is string)
                {
                    RecordIdString = (string)recordIdParam;
                }
            }
        }
    }
}