define("SibIntegrationLogItemBasePage", ["SibIntegrationLogItemBasePageResources", "BusinessRuleModule"],
	function(Resources, BusinessRuleModule) {
	    return {
	        entitySchemaName: "SibIntegrationLogItemBase",
	        messages: {},
	        attributes: {
				"InboundMessageLink": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
	            "OutboundMessageLink": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
				"CreatedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
	            "ModifiedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
	            "ExternalStartedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            },
	            "ExternalEndedOnExt": {
	                type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
	                dataValueType: Terrasoft.DataValueType.TEXT,
	                value: ""
	            }
			},
	        diff: /* SCHEMA_DIFF */[
				{
	                "operation": "insert",
	                "name": "RecordType",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 0, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "Direction",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 1, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false,
	                }
	            },
				{
	                "operation": "insert",
	                "name": "Id",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 2, "colSpan": 12, "rowSpan": 1},
	                    "dataValueType": Terrasoft.DataValueType.TEXT,
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "RecordId",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 3, "colSpan": 12, "rowSpan": 1},
	                    "dataValueType": Terrasoft.DataValueType.TEXT,
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "SibIntegrationStatus",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 4, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
				{
	                "operation": "insert",
	                "name": "CreatedBy",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 0, "colSpan": 12, "rowSpan": 1},
	                    "enabled": false
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "CreatedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 1, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.CreatedOnCaption"},
	                    "enabled": false
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "ModifiedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 12, "row": 2, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.EndedOnCaption"},
	                    "enabled": false
	                },
	            },
	            {
	                "operation": "insert",
	                "name": "ExternalStartedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 18, "row": 1, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.ExternalStartedOnCaption"},
	                    "enabled": false
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "ExternalEndedOnExt",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 18, "row": 2, "colSpan": 6},
	                    "caption": {"bindTo": "Resources.Strings.ExternalEndedOnCaption"},
	                    "enabled": false
	                }
	            },
				{
	            	"operation": "insert",
	            	"name": "OutboundMessageLink",
	                "parentName": "Header",
	            	"propertyName": "items",
	            	"values": {
	                    "caption": {"bindTo": "Resources.Strings.OutboundMessageCaption"},
	            		"showValueAsLink": true,
	            		"controlConfig": {
	            			"enabled": false,
	            			"href": {
	            				"bindTo": "getOutboundMessageLink"
	            			},
	            			"linkclick": {
	            				"bindTo": "onOutboundMessageLinkClick"
	            			}
	            		},
	            		"layout": {
	            			"column": 0, "row": 5, "colSpan": 12, "rowSpan": 1,
	            			"layoutName": "Header"
	            		},
	            		"labelConfig": {},
	            		"enabled": false,
	            		"bindTo": "OutboundMessageLink"
	            	}
	            },
	            {
	            	"operation": "insert",
	            	"name": "InboundMessageLink",
	                "parentName": "Header",
	            	"propertyName": "items",
	            	"values": {
	                    "caption": {"bindTo": "Resources.Strings.InboundMessageCaption"},
	            		"showValueAsLink": true,
	            		"controlConfig": {
	            			"enabled": false,
	            			"href": {
	            				"bindTo": "getInboundMessageLink"
	            			},
	            			"linkclick": {
	            				"bindTo": "onInboundMessageLinkClick"
	            			}
	            		},
	            		"layout": {
	            			"column": 12, "row": 5, "colSpan": 12, "rowSpan": 1,
	            			"layoutName": "Header"
	            		},
	            		"labelConfig": {},
	            		"enabled": false,
	            		"bindTo": "InboundMessageLink"
	            	}
	            },
	            {
	                "operation": "insert",
	                "name": "ErrorText",
	                "parentName": "Header",
	                "propertyName": "items",
	                "values": {
	                    "layout": {"column": 0, "row": 6, "colSpan": 24, "rowSpan": 3},
	                    "enabled": false,
	                    "controlConfig": {
	                        "height": "96px"
	                    },
	                    "contentType": Terrasoft.ContentType.LONG_TEXT
	                },

	            },
			],/* SCHEMA_DIFF */
	        methods: {
				/*
	            * overriden
	            */
	            onEntityInitialized: function () {
	                this.callParent(arguments);
	                this.getMessageLinks();
	                this.updateExtFields();
	            },

				getMessageLinks: function() {
	                var recordId = this.get("Id");
	                if (!recordId) {
	                    return "";
	                }

	                var inboundLink;
	                if (this.get("MessagesStorageMethod") == 0) {
	                    inboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFile/" +
	                        this.entitySchemaName + "/InboundMessage/in/" + recordId;
	                } else {
	                    inboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFileFromFileSystem/" +
	                        this.entitySchemaName + "/InboundMessage/in/" + recordId;
	                }
	                this.set("InboundMessageLink", inboundLink);

	                var outboundLink;
	                if (this.get("MessagesStorageMethod") == 0) {
	                    outboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFile/" +
	                        this.entitySchemaName + "/OutboundMessage/out/" + recordId;
	                } else {
	                    outboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/GetFileFromFileSystem/" +
	                        this.entitySchemaName + "/OutboundMessage/out/" + recordId;
	                }
	                this.set("OutboundMessageLink", outboundLink);
	            },

				getInboundMessageLink: function() {
	        		return this.getLink(this.get("InboundMessageLink"), this.get("Resources.Strings.InboundMessageCaption"));
	        	},

	            getOutboundMessageLink: function() {
	                return this.getLink(this.get("OutboundMessageLink"), this.get("Resources.Strings.OutboundMessageCaption"));
	            },

	        	onInboundMessageLinkClick: function() {
	        		return;
	        	},

	            onOutboundMessageLinkClick: function() {
	        		return;
	        	},

	        	getLink: function(link, caption) {
	        		if (Terrasoft.isUrl(link)) {
	        			return {
	        				url: link,
	        				caption: caption
	        			};
	        		}
	        	},

	            updateExtFields: function() {
	                this.set("CreatedOnExt", this.getFullDateTimeString(this.get("CreatedOn")));
	                this.set("ModifiedOnExt", this.getFullDateTimeString(this.get("ModifiedOn")));
	                this.set("ExternalStartedOnExt", this.getFullDateTimeString(this.get("ExternalStartedOn")));
	                this.set("ExternalEndedOnExt", this.getFullDateTimeString(this.get("ExternalEndedOn")));
	            },

	            getFullDateTimeString: function(dateTimeValue) {
	                if (!dateTimeValue) return null;
	                return dateTimeValue.toLocaleDateString() + '    ' + dateTimeValue.toLocaleTimeString()  + '.' +
	                    dateTimeValue.getMilliseconds();
	            }
			},
	        rules: {
				"ExternalStartedOnExt": {
	                "ExternalStartedOnExtVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "ExternalStartedOnExt"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "ExternalEndedOnExt": {
	                "ExternalEndedOnExtVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "ExternalEndedOnExt"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "ErrorText": {
	                "ErrorTextVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "ErrorText"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "OutboundMessageLink": {
	                "OutboundMessageLinkVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "OutboundMessage"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            },
	            "InboundMessageLink": {
	                "InboundMessageLinkVisibile": {
	                    "ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
	                    "property": BusinessRuleModule.enums.Property.VISIBLE,
	                    "conditions": [{
	                        "leftExpression": {
	                            "type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
	                            "attribute": "InboundMessage"
	                        },
	                        "comparisonType": Terrasoft.ComparisonType.IS_NOT_NULL
	                    }]
	                }
	            }
			}
	    };
	});
