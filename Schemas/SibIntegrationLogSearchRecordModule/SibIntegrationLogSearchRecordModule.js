define("SibIntegrationLogSearchRecordModule", ["ModalBox", "BaseSchemaModuleV2"],
       function(ModalBox) {
   Ext.define("Terrasoft.configuration.SibIntegrationLogSearchRecordModule", {
       extend: "Terrasoft.BaseSchemaModule",
       alternateClassName: "Terrasoft.SibIntegrationLogSearchRecordModule",
       /**
        * @inheritDoc Terrasoft.BaseSchemaModule#generateViewContainerId
        * @overridden
        */
       generateViewContainerId: false,
       /**
        * @inheritDoc Terrasoft.BaseSchemaModule#initSchemaName
        * @overridden
        */
       initSchemaName: function() {
           this.schemaName = "SibIntegrationLogSearchRecordModalBox";
       },
       /**
        * @inheritDoc Terrasoft.BaseSchemaModule#initHistoryState
        * @overridden
        */
       initHistoryState: Terrasoft.emptyFn,
   });
   return Terrasoft.SibIntegrationLogSearchRecordModule;
});
