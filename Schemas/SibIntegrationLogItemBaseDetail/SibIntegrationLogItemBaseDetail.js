define("SibIntegrationLogItemBaseDetail", ["SibIntegrationLogItemBaseDetailResources", "ConfigurationEnums", "GridUtilitiesV2",
	"css!SibIntegrationLogItemBaseDetailCss"],
	function(Resources, enums) {
	    return {
	        entitySchemaName: "SibIntegrationLogItemBase",
	        details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
	        attributes: {
				"UsedEntities": {
					dataValueType: Terrasoft.DataValueType.COLLECTION,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
					/*
					value: {
						"Контрагент": {
							entityName: "Account",
							pageName: "AccountPageV2",
							integrationIdColumn: "SibMDMIntegrationId",
							getCustomFilter: function(integrationId, scope) {
								return scope.getMDMFilter(integrationId);
							}
						},
						...
					}
					*/
				}
			},
	        methods: {
	            /*
	            * overriden
	            */
	            initQueryColumns: function(entitySchemaQuery) {
	                this.callParent(arguments);
	                entitySchemaQuery.addColumn("Direction");
					entitySchemaQuery.addColumn("MessagesStorageMethod");
	            },

	            /*
	            * overriden
	            */
	            addRecordOperationsMenuItems: this.Terrasoft.emptyFn,

	            /*
	            * overriden
	            */
	            onGridDataLoaded: function () {
	               this.callParent(arguments);
	               this.gridCorrectDisplayData();
	            },

	            /*
	            * overriden
	            */
	            addColumnLink: function(item, column) {
	                var columnPath = column.columnPath;
	                var scope, columnIdName, entitySchemaName;
	                if (columnPath === "OutboundMessage" || columnPath == "InboundMessage") {
	                    scope = this;
	                    columnIdName = item.primaryColumnName;
	                    entitySchemaName = this.entitySchemaName;
	                    item["on" + columnPath + "LinkClick"] = function() {
	                        var recordId = item.get("Id");
							var serviceName;
	                        if (!recordId) {
	                                return "";
	                        }
	                        var displayValue = item.get(columnPath);
	                        if (scope.Ext.isEmpty(displayValue)) {
	                            return "";
	                        }

							if (item.get("MessagesStorageMethod") == 0) {
								serviceName = "GetFile";
							} else {
								serviceName = "GetFileFromFileSystem";
							}
	                        var inboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/" + serviceName + "/" +
	                            entitySchemaName + "/InboundMessage/in/" + recordId;
	                        var outboundLink = Terrasoft.workspaceBaseUrl + "/rest/SibIntegrationLogFileHelper/" + serviceName + "/" +
	                            entitySchemaName + "/OutboundMessage/out/" + recordId;
	                        var link = (columnPath == "OutboundMessage") ? outboundLink: inboundLink;
	                        return {
	                            caption: displayValue,
	                            target: "_self",
	                            title: displayValue,
	                            url: link
	                        };
	                    };
	                } else {
	                    this.callParent(arguments);
	                }
	            },

				/*
				* overriden
				*/
				onActiveRowAction: function(buttonTag, primaryColumnValue) {
	                if (buttonTag == 'gotoRecord') {
	                    var gridData = this.getGridData();
	                    var activeRow = gridData.get(primaryColumnValue);
	                    var recordId = activeRow.get("RecordId");
	                    var recordType = activeRow.get("RecordType");
	                    var pageSchemaName = this.get("UsedEntities")[recordType].pageName;
	                    var entitySchemaName = this.get("UsedEntities")[recordType].entityName;
						var integrationIdColumnName = this.get("UsedEntities")[recordType].integrationIdColumn;
						var filter = this.get("UsedEntities")[recordType].getCustomFilter;
	                    if (!pageSchemaName || !entitySchemaName) return;

	                    var esq;
	                    if (activeRow.get("Direction") == "Исходящая") {
	                        esq = this.checkRecordExistsEsq(entitySchemaName, recordId);
	                        esq.getEntityCollection(function(response) {
	                            if(response && response.success && response.collection.getCount() > 0) {
	                                this.sandbox.publish("PushHistoryState",
										{hash: Terrasoft.combinePath("CardModuleV2", pageSchemaName, "edit", recordId)}
									);
	                            } else {
	                                this.showInformationDialog(
										Ext.String.format(this.get("Resources.Strings.GoToRecordErrorMessage"), "Id", recordId)
									);
	                            }
	                        }, this);
	                    } else {
	                        esq = this.getRecordByIntegrationIdEsq(entitySchemaName, recordId, integrationIdColumnName, filter);
	                        esq.getEntityCollection(function(response) {
	                            if (response && response.success && response.collection.getCount() > 0) {
	                                var id = response.collection.getByIndex(0).get("Id");
	                                this.sandbox.publish("PushHistoryState",
										{hash: Terrasoft.combinePath("CardModuleV2", pageSchemaName, "edit", id)}
									);
	                            } else {
	                                this.showInformationDialog(this.get("Resources.Strings.GoToRecordSimpleErrorMessage"));
	                            }
	                        }, this);
	                    }
	                } else if (buttonTag == 'edit') {
						this.editRecord();
					} else {
                        this.callParent(arguments);
                    }
	            },

				getDefaultValueByName: function(valueName) {
					var defaultValues = this.get("DefaultValues") || this.getDefaultValues();
					if (Ext.isEmpty(defaultValues)) {
						return null;
					}
					var defaultValue = Terrasoft.findItem(defaultValues, {name: valueName});
					if (!defaultValue) {
						return null;
					}
					defaultValue = defaultValue.item;
					if (!defaultValue || Ext.isEmpty(defaultValue.value)) {
						return null;
					}
					return defaultValue.value;
				},

	            gridCorrectDisplayData: function () {
	                var gridData = this.getGridData();
	                var items = gridData.getItems();
	                var loadedObject = {};
	                Terrasoft.each(items, function (item) {
	                    item.customStyle = null;
	                    if (item.get("OutboundMessage")) {
	                        item.set("OutboundMessage", this.get("Resources.Strings.GeXMLFile"));
	                    }
	                    if (item.get("InboundMessage")) {
	                        item.set("InboundMessage", this.get("Resources.Strings.GeXMLFile"));
	                    }
	                    var primaryValue = item.get(item.primaryColumnName);
	                    loadedObject[primaryValue] = item;
	                }, this);
	                gridData.clear();
	                gridData.loadAll(loadedObject);
	            },

				checkRecordExistsEsq: function (entitySchemaName, id) {
					esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: entitySchemaName
					});
					esq.addColumn("Id");
					esq.filters.add("IdFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", id));
					return esq;
				},

				getRecordByIntegrationIdEsq: function (entitySchemaName, integrationId, integrationIdColumnName, filter) {
					esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: entitySchemaName
					});
					esq.addColumn("Id");
					if (!filter) {
						esq.filters.add("IntegrationIdFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, integrationIdColumnName, integrationId));
					} else {
						esq.filters.add(filter(integrationId, this));
					}
					return esq;
				}
	        },
	        diff: /**SCHEMA_DIFF*/[
	            {
	                "operation": "merge",
	                "name": "DataGrid",
	                "values": {
	                    "activeRowAction": {"bindTo": "onActiveRowAction"},
	                    "activeRowActions": []
	                }
	            },
				{
	                "operation": "insert",
	                "name": "EditButton",
	                "parentName": "DataGrid",
	                "propertyName": "activeRowActions",
	                "values": {
	                    "id": "EditActionButton",
	                    "className": "Terrasoft.Button",
	                    "style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
	                    "visible": true,
	                    "caption": Resources.localizableStrings.EditButtonCaption,
	                    "tag": "edit"
	                }
	            },
	            {
	                "operation": "insert",
	                "name": "GoToRecordButton",
	                "parentName": "DataGrid",
	                "propertyName": "activeRowActions",
	                "values": {
	                    "id": "GoToRecordActionButton",
	                    "className": "Terrasoft.Button",
	                    "style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
	                    "visible": true,
	                    "caption": Resources.localizableStrings.GoToButtonCaption,
	                    "tag": "gotoRecord"
	                }
	            }
	        ]/**SCHEMA_DIFF*/
	    };
	});
