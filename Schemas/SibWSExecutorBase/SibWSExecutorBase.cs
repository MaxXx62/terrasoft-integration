using System;
using System.Collections.Generic;
using Terrasoft.Core;

namespace SiblionIntegration
{
    public abstract class SibWSExecutorBase
    {
        private UserConnection _userConnection;
        private Guid ChildLogId { get; set; }
        private string LogAdditionalInfo { get; set; }

        protected UserConnection UserConnection { get => _userConnection; }
        protected ServiceInfo ServiceInfo { get; private set; }
        protected Exception ErrorException { get; set; }
        

        public Dictionary<string, object> Parameters { get; set; } = new Dictionary<string, object>();
        public SibIntegrationLogService LogService { get; set; }
        public bool ExternalLogOption { get; set; } = false;
        public string LogServiceName { get; set; }

        public SibWSExecutorBase(UserConnection userConnection, ServiceInfo serviceInfo)
        {
            _userConnection = userConnection;
            ServiceInfo = serviceInfo;
        }

        public override string ToString()
        {
            return ServiceInfo.Name;
        }

        public abstract object Execute();

        protected void CreatePrimaryLogRecord()
        {
            if (LogService == null) InitLogService();

            if (Parameters.ContainsKey("LogService.LogLevel")) LogService.LogLevel = (int)Parameters["LogService.LogLevel"];
            if (Parameters.ContainsKey("LogService.XMLMaxLength")) LogService.XMLMaxLength = (int)Parameters["LogService.XMLMaxLength"];

            if (!ExternalLogOption) LogService.CreateLogRecord(ServiceInfo.Name, ServiceInfo.Code, ServiceInfo.DirectionName);
        }

        protected void CreateChildLogRecord(string recordId, string recordType, string direction)
        {
            ChildLogId = LogService.CreateVirtualLogItemRecord(recordId, recordType, direction);
        }

        protected void SaveRecordCountToPrimaryLog(int recordCount, bool isAdd = false)
        {
            LogService.SetRecordCountToLog(recordCount, isAdd);
        }

        protected void SaveOutboundMessageToPrimaryLog(object messageObject)
        {
            if (!ExternalLogOption) LogService.SetOutboundMessageToLog(messageObject);
        }

        protected void SaveInboundMessageToPrimaryLog(object messageObject)
        {
            if (!ExternalLogOption) LogService.SetInboundMessageToLog(messageObject);
        }

        protected void SaveOutboundMessageToChildLog(object messageObject)
        {
            LogService.SetOutboundMessageToVirtualLogItem(ChildLogId, messageObject);
        }

        protected void SaveInboundMessageToChildLog(object messageObject)
        {
            LogService.SetInboundMessageToVirtualLogItem(ChildLogId, messageObject);
        }

        /// <summary>
        /// �������� ������ ���� ��������� ����������
        /// </summary>
        /// <param name="recordId">Id ������</param>
        /// <param name="recordType">��� ������</param>
        /// <param name="direction">����������� ���������� ('��������' ��� '���������')</param>
        /// <param name="requestObject">��������� ���������</param>
        /// <param name="responseObject">�������� ���������</param>
        /// <param name="errorException">���������� (������ �� ������)</param>
        public void CreateAndCloseChildLogRecord(string recordId, string recordType, string direction, 
            object requestObject, object responseObject, Exception errorException = null)
        {
            LogService.CreateAndCloseLogItemRecord(recordId, recordType, direction, requestObject, responseObject, errorException);
        }

        /// <summary>
        /// ���������� ���������� � ���� '�������������� ��������' � �������� ������ ����
        /// </summary>
        /// <param name="info">��������</param>
        public void SaveAdditionalInfoToLog(string info)
        {
            LogAdditionalInfo = info;
        }

        protected void SaveExternalStartTimeToLog()
        {
            if (ChildLogId == Guid.Empty) 
                LogService.SetExternalTimeToLog(isStart: true);
            else
                LogService.SetExternalTimeToVirtualLogItem(ChildLogId, isStart: true);
        }

        protected void SaveExternalEndTimeToLog()
        {
            if (ChildLogId == Guid.Empty) 
                LogService.SetExternalTimeToLog(isStart: false);
            else
                LogService.SetExternalTimeToVirtualLogItem(ChildLogId, isStart: false);
        }

        protected void ClosePrimaryLogRecord()
        {
            if (!ExternalLogOption) LogService.CloseLogRecord(ErrorException, LogAdditionalInfo);
        }

        protected void CloseChildLogRecord()
        {
            LogService.SaveVirtualLogItemRecord(ChildLogId, ErrorException);
        }

        private void InitLogService()
        {
            LogService = SibIntegrationLogService.CreateLogService(LogServiceName, _userConnection);
        }

        protected void OptionalOverrideServiceInfo()
        {
            if (Parameters.ContainsKey("ServiceInfo.Name")) ServiceInfo.Name = (string)Parameters["ServiceInfo.Name"];
            if (Parameters.ContainsKey("ServiceInfo.Code")) ServiceInfo.Code = (string)Parameters["ServiceInfo.Code"];
            if (Parameters.ContainsKey("ServiceInfo.Direction")) ServiceInfo.Direction = (WSDirection)Parameters["ServiceInfo.Direction"];
            if (Parameters.ContainsKey("ServiceInfo.URL")) ServiceInfo.URL = (string)Parameters["ServiceInfo.URL"];
            if (Parameters.ContainsKey("ServiceInfo.UserName")) ServiceInfo.UserName = (string)Parameters["ServiceInfo.UserName"];
            if (Parameters.ContainsKey("ServiceInfo.Password")) ServiceInfo.Password = (string)Parameters["ServiceInfo.Password"];
            if (Parameters.ContainsKey("ServiceInfo.Timeout")) ServiceInfo.Timeout = (int)Parameters["ServiceInfo.Timeout"];
        }
    }
}