using System;
using System.Collections.Generic;
using System.Linq;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.Entities;

namespace SiblionIntegration
{
    public abstract class SibAdvancedDequeueWorkerBase
    {
        protected UserConnection _userConnection;
        protected List<TimeModelItem> TimeModel { get; set;}
        protected abstract string EntityName { get; set;}
        protected abstract string TypeColumnName { get; set; }

        protected SibAdvancedDequeueWorkerBase(UserConnection userConnection)
        {
            _userConnection = userConnection;
            InitTimeModel();

            if (TimeModel == null || TimeModel.Count == 0)
                throw new Exception("Свойство 'TimeModel' не было инициализировано.");
        }

        protected virtual void InitTimeModel()
        {
            TimeModel = new List<TimeModelItem>
            {
                {new TimeModelItem {Number = 0, Timeline = new TimeSpan(0, 30, 0), Operator = ">=", Period = new TimeSpan(0)}},             //SibModifiedOn >= Текущее время - 30 мин
                {new TimeModelItem {Number = 1, Timeline = new TimeSpan(1, 0, 0), Operator = ">=", Period = new TimeSpan(0, 10, 0)}},       //(SibModifiedOn >= Текущее время - 1 час) и (LastTryOn <= Текущее время - 10 мин.)
                {new TimeModelItem {Number = 2, Timeline = new TimeSpan(2, 0, 0), Operator = ">=", Period = new TimeSpan(0, 30, 0)}},       //(SibModifiedOn >= Текущее время - 2 часа) и (LastTryOn <= Текущее время - 30 мин.)
                {new TimeModelItem {Number = 3, Timeline = new TimeSpan(5, 0, 0), Operator = ">=", Period = new TimeSpan(1, 0, 0)}},        //(SibModifiedOn >= Текущее время - 5 часов) и (LastTryOn <= Текущее время - 60 мин.)
                {new TimeModelItem {Number = 4, Timeline = new TimeSpan(11, 0, 0), Operator = ">=", Period = new TimeSpan(2, 0, 0)}},       //(SibModifiedOn >= Текущее время - 11 часов) и (LastTryOn <= Текущее время - 120 мин.)
                {new TimeModelItem {Number = 5, Timeline = new TimeSpan(16, 0, 0), Operator = ">=", Period = new TimeSpan(4, 0, 0)}},       //(SibModifiedOn >= Текущее время - 16 часов) и (LastTryOn <= Текущее время - 240 мин.)
                {new TimeModelItem {Number = 6, Timeline = new TimeSpan(16, 0, 0), Operator = "<", Period = new TimeSpan(6, 0, 0)}}         //(SibModifiedOn < Текущее время - 16 часов) и (LastTryOn <= Текущее время - 360 мин.)
            };
        }

        /// <summary>
        /// Получение записей из очереди
        /// </summary>
        /// <param name="typeId">Id типа записи очереди</param>
        /// <param name="count">Кол-во возвращаемых записей</param>
        /// <param name="isFirstPriorityOnly">Признак возврата записей только с первым приоритетом</param>
        /// <returns></returns>
        public List<Guid> GetRecordsFromQueue(Guid typeId, int count = 1, bool isFirstPriorityOnly = false)
        {
            var fpRecordIds = GetFirstPriorityRecord(typeId, count);
            if (fpRecordIds.Count < count && !isFirstPriorityOnly)
            {
                var spRecordIds = GetSecondPriorityRecord(typeId, count - fpRecordIds.Count);
                fpRecordIds.AddRange(spRecordIds);
            }

            return fpRecordIds;
        }

        private List<Guid> GetFirstPriorityRecord(Guid typeId, int count)
        {
            var result = new List<Guid>();

            var esq = GetFirstPriorityRecordEsq(typeId, count);
            var entityCollection = esq.GetEntityCollection(_userConnection);
            if (entityCollection.Count > 0)
            {
                result = entityCollection.Select(x => x.PrimaryColumnValue).ToList();
            }

            return result;
        }

        private List<Guid> GetSecondPriorityRecord(Guid typeId, int count)
        {
            var result = new List<Guid>();

            var esq = GetSecondPriorityRecordEsq(typeId, count);
            var entityCollection = esq.GetEntityCollection(_userConnection);
            foreach (var queueItem in entityCollection)
            {
                if (result.Count >= count) break;

                var lastTry = queueItem.GetTypedColumnValue<DateTime>("LastTryOn");
                var modifiedByUser = queueItem.GetTypedColumnValue<DateTime>("SibModifiedOn");
                if (IsReadyForProcess(TimeModel, modifiedByUser, lastTry))
                {
                    result.Add(queueItem.PrimaryColumnValue);
                }
                else
                {
                    SetErrorToQueueItem(queueItem);
                }
            }

            return result;
        }

        private bool IsReadyForProcess(List<TimeModelItem> timeModel, DateTime modifiedByUser, DateTime lastTry)
        {
            var result = false;
            foreach (var modelItem in timeModel.OrderBy(x => x.Number))
            {
                if (TimeModelItem.CompareMethod[modelItem.Operator](modifiedByUser, DateTime.Now - modelItem.Timeline) &&
                    lastTry <= DateTime.Now - modelItem.Period)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private static void SetErrorToQueueItem(Entity queueItem)
        {
            queueItem.SetColumnValue("IsError", true);
            queueItem.Save(false);
        }

        protected virtual EntitySchemaQuery GetFirstPriorityRecordEsq(Guid typeId, int count)
        {
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, EntityName);
            esq.PrimaryQueryColumn.IsAlwaysSelect = true;
            var sibModifiedOnColumn = esq.AddColumn("SibModifiedOn");
            var priorityColumn = esq.AddColumn("Priority");
            priorityColumn.OrderByDesc(0);
            sibModifiedOnColumn.OrderByAsc(1);
            esq.RowCount = count;
            esq.Filters.AddRange(new List<IEntitySchemaQueryFilterItem> {
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, TypeColumnName, typeId),
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "IsInUse", false),
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "ErrorCount", 0)
            });

            return esq;
        }

        protected virtual EntitySchemaQuery GetSecondPriorityRecordEsq(Guid typeId, int count)
        {
            var esq = new EntitySchemaQuery(_userConnection.EntitySchemaManager, EntityName);
            esq.PrimaryQueryColumn.IsAlwaysSelect = true;
            esq.AddColumn("IsError");
            esq.AddColumn("LastTryOn");
            var sibModifiedOnColumn = esq.AddColumn("SibModifiedOn");
            sibModifiedOnColumn.OrderByDesc(0);
            esq.Filters.AddRange(new List<IEntitySchemaQueryFilterItem> {
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, TypeColumnName, typeId),
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "IsInUse", false),
                esq.CreateFilterWithParameters(FilterComparisonType.NotEqual, "ErrorCount", 0),
                esq.CreateFilterWithParameters(FilterComparisonType.Equal, "IsError", false)
            });

            return esq;
        }

        protected class TimeModelItem
        {
            public int Number { get; set; }
            public TimeSpan Timeline { get; set; }
            public string Operator { get; set; }
            public TimeSpan Period { get; set; }

            public static readonly Dictionary<string, Func<DateTime, DateTime, bool>> CompareMethod =
                new Dictionary<string, Func<DateTime, DateTime, bool>>
                {
                    ["<="] = (x, y) => { return x <= y; },
                    [">="] = (x, y) => { return x >= y; },
                    ["<"] = (x, y) => { return x < y; },
                    [">"] = (x, y) => { return x > y; }
                };
        }
    }
}