using System;
using Terrasoft.Core;

namespace SiblionIntegration
{
    public abstract class SibWSOutboundExecutorBase: SibWSExecutorBase
    {
        public SibWSOutboundExecutorBase(UserConnection userConnection, ServiceInfo serviceInfo) : base(userConnection, serviceInfo)
        {
        }

        protected abstract object PrepareData();
        protected abstract object CallWebService(object parameter);
        protected abstract object ProcessData(object parameter);

        protected string ConvertValueToString(object value)
        {
            if (value is Guid && (Guid)value == Guid.Empty)
            {
                return "";
            }
            else if (value is string && String.IsNullOrEmpty((string)value))
            {
                return "";
            }
            else
            {
                return (value != null ? value.ToString() : "");
            }
        }
    }
}