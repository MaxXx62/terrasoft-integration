define("SibIntegrationLogSearchRecordModalBox", ["ModalBox", "css!SibIntegrationLogSearchRecordModalBoxCss"], function(ModalBox) {
	return {
		attributes: {
			"RecordPrimaryId": {
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: Terrasoft.DataValueType.TEXT
			},
			"RecordSecondaryId": {
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: Terrasoft.DataValueType.TEXT
			}
		},
		messages: {
			"RecordIdFromModal": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		methods: {

			onApplyButtonClick: function() {
				this.sandbox.publish("RecordIdFromModal", {
					recordPrimaryId: this.get("RecordPrimaryId"),
					recordSecondaryId: this.get("RecordSecondaryId")
				}, [this.sandbox.id]);
				ModalBox.close();
			},

			onCloseButtonClick: function() {
				ModalBox.close();
			},
		},
		diff: [
			{
				"operation": "insert",
				"name": "sibRecordIdBoxContainer",
				"index": 0,
				"propertyName": "items",
				"markerValue": "PaymentslContractSignDateBoxContainer",
				"values": {
					"id": "sibRecordIdBoxContainer",
					"selectors": {"wrapEl": "#sibRecordIdBoxContainer"},
					"wrapClass": ["probabilyty-box-container"],
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"items": []
				}
			},

			{
				"operation": "insert",
				"parentName": "sibRecordIdBoxContainer",
				"propertyName": "items",
				"name": "sibRecordIdBoxContainer_Layout",
				"values": {
					"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
					"items": []
				}
			},

			{
				"operation": "insert",
				"propertyName": "items",
				"name": "headerContainer",
				"parentName": "sibRecordIdBoxContainer_Layout",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["header-container"],
					"items": [],
					"layout": {"column": 0, "row": 0, "colSpan": 16},
				}
			},

			{
				"operation": "insert",
				"parentName": "headerContainer",
				"propertyName": "items",
				"name": "HeaderLabel",
				"values": {
					"itemType": Terrasoft.ViewItemType.LABEL,
					"caption": {"bindTo": "Resources.Strings.HeaderCaption"},
				}
			},

			{
				"operation": "insert",
				"parentName": "sibRecordIdBoxContainer_Layout",
				"propertyName": "items",
				"name": "RecordPrimaryId",
				"values": {
					"bindTo": "RecordPrimaryId",
					"labelConfig": {
					    "visible": false
					},
					"controlConfig": {
						"focused": true
					},
					"layout": {"column": 0, "row": 1, "colSpan": 16},
				}
			},

			{
				"operation": "insert",
				"parentName": "sibRecordIdBoxContainer_Layout",
				"propertyName": "items",
				"name": "RecordSecondaryId",
				"values": {
					"bindTo": "RecordSecondaryId",
					"labelConfig": {
					    "visible": false
					},
					"layout": {"column": 0, "row": 2, "colSpan": 16},
				}
			},

			{
				"operation": "insert",
				"propertyName": "items",
				"name": "buttonsContainer",
				"index": 2,
				"parentName": "sibRecordIdBoxContainer_Layout",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"wrapClass": ["buttons-container"],
					"items": [],
					"layout": {"column": 0, "row": 3, "colSpan": 16},
				}
			},

			{
				"operation": "insert",
				"parentName": "buttonsContainer",
				"name": "ApplyButton",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"click": {"bindTo": "onApplyButtonClick"},
					"markerValue": "ApplyButton",
					"caption": {"bindTo": "Resources.Strings.ApplyButtonCaption"}
				}
			},
			{
				"operation": "insert",
				"parentName": "buttonsContainer",
				"name": "CloseButton",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
					"click": {"bindTo": "onCloseButtonClick"},
					"markerValue": "CloseButton",
					"caption": {"bindTo": "Resources.Strings.CancelButtonCaption"}
				}
			}
		]
	};
});
