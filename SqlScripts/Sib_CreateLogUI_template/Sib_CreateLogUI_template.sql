/*

DECLARE @LogEntitySchemaName nchar(100)
DECLARE @LogItemEntitySchemaName nchar(100)
DECLARE @ModuleCaption nchar(100)
DECLARE @SectionSchemaName nchar(100)
DECLARE @CardSchemaName nchar(100)
DECLARE @DetailCaption nchar(100)
DECLARE @DetailPageCaption nchar(100)
DECLARE @DetailSchemaName nchar(100)
DECLARE @DetailCardSchemaName nchar(100)
DECLARE @SysModuleEntityId uniqueidentifier
DECLARE @SectionSchemaId uniqueidentifier

SET @LogEntitySchemaName = 'SibLeadIntegrationLog'
SET @LogEntitySchemaName = 'SibLeadIntegrationLogItem'
SET @SectionSchemaName = 'SibLeadIntegrationLogSection'
SET @CardSchemaName = 'SibLeadIntegrationLogPage'
SET @DetailSchemaName = 'SibLeadIntegrationLogItemDetail'
SET @DetailCardSchemaName = 'SibLeadIntegrationLogItemPage'
SET @ModuleCaption = 'Лог интеграции лидов'
SET @DetailCaption = 'Лог элементов интеграции'
SET @DetailPageCaption = 'Лог элементов интеграции лидов'


SET @SysModuleEntityId = NEWID()


INSERT INTO SysModuleEntity (Id,
ProcessListeners,
SysEntitySchemaUId)
OUTPUT INSERTED.Id
  VALUES (@SysModuleEntityId, 0, (SELECT TOP 1 UId FROM SysSchema WHERE Name = @LogEntitySchemaName));

SELECT TOP 1
  @SectionSchemaId = Uid
FROM SysSchema
WHERE Name = @SectionSchemaName;

INSERT INTO SysModule ("Caption", "SysModuleEntityId", "FolderModeId", "Code", "ModuleHeader", "SectionModuleSchemaUId", "SectionSchemaUId")
  VALUES (@ModuleCaption, @SysModuleEntityId, 'A24A734D-3955-E011-981F-00155D043204', @LogEntitySchemaName, @ModuleCaption, 'DF58589E-26A6-44D1-B8D4-EDF1734D02B4', @SectionSchemaId);

INSERT INTO SysModuleEdit (SysModuleEntityId,
UseModuleDetails,
Position,
HelpContextId,
ProcessListeners,
CardSchemaUId,
ActionKindCaption,
ActionKindName,
PageCaption)
  VALUES ((SELECT TOP 1 Id FROM SysModuleEntity WHERE SysEntitySchemaUId = (SELECT TOP 1 UId FROM SysSchema WHERE Name = @LogEntitySchemaName)), 1, 0, '', 0, (SELECT TOP 1 UId FROM SysSchema WHERE name = @CardSchemaName), '', '', @ModuleCaption)

INSERT INTO SysDetail (ProcessListeners,
Caption,
DetailSchemaUId,
EntitySchemaUId)
  VALUES (0, @DetailCaption, (SELECT TOP 1 UId FROM SysSchema WHERE name = @DetailSchemaName), (SELECT TOP 1 UId FROM SysSchema WHERE name = @LogEntitySchemaName));

INSERT INTO SysModuleEntity (ProcessListeners,
SysEntitySchemaUId)
  VALUES (0, (SELECT TOP 1 UId FROM SysSchema WHERE Name = @LogItemEntitySchemaName))

INSERT INTO SysModuleEdit (SysModuleEntityId,
UseModuleDetails,
Position,
HelpContextId,
ProcessListeners,
CardSchemaUId,
ActionKindCaption,
ActionKindName,
PageCaption)
  VALUES ((SELECT TOP 1 Id FROM SysModuleEntity WHERE SysEntitySchemaUId = (SELECT TOP 1 UId FROM SysSchema WHERE Name = @LogItemEntitySchemaName)), 1, 0, '', 0, (SELECT TOP 1 UId FROM SysSchema WHERE name = @DetailCardSchemaName), '', '', @DetailPageCaption)
  
*/