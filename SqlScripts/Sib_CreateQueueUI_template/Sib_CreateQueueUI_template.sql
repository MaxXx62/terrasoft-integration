/*

DECLARE @QueueEntitySchemaName nchar(100)
DECLARE @ModuleCaption nchar(100)
DECLARE @SectionSchemaName nchar(100)
DECLARE @CardSchemaName nchar(100)
DECLARE @SysModuleEntityId uniqueidentifier
DECLARE @SectionSchemaId uniqueidentifier

SET @QueueEntitySchemaName = 'Sib1CImportQueue'
SET @SectionSchemaName = 'Sib1CImportQueueSection'
SET @CardSchemaName = 'Sib1CImportQueuePage'
SET @ModuleCaption = 'Очередь импорта из 1С'


SET @SysModuleEntityId = NEWID()


INSERT INTO SysModuleEntity (Id,
ProcessListeners,
SysEntitySchemaUId)
OUTPUT INSERTED.Id
  VALUES (@SysModuleEntityId, 0, (SELECT TOP 1 UId FROM SysSchema WHERE Name = @QueueEntitySchemaName));

SELECT TOP 1
  @SectionSchemaId = Uid
FROM SysSchema
WHERE Name = @SectionSchemaName;

INSERT INTO SysModule ("Caption", "SysModuleEntityId", "FolderModeId", "Code", "ModuleHeader", "SectionModuleSchemaUId", "SectionSchemaUId")
  VALUES (@ModuleCaption, @SysModuleEntityId, 'A24A734D-3955-E011-981F-00155D043204', @QueueEntitySchemaName, @ModuleCaption, 'DF58589E-26A6-44D1-B8D4-EDF1734D02B4', @SectionSchemaId);

INSERT INTO SysModuleEdit (SysModuleEntityId,
UseModuleDetails,
Position,
HelpContextId,
ProcessListeners,
CardSchemaUId,
ActionKindCaption,
ActionKindName,
PageCaption)
  VALUES ((SELECT TOP 1 Id FROM SysModuleEntity WHERE SysEntitySchemaUId = (SELECT TOP 1 UId FROM SysSchema WHERE Name = @QueueEntitySchemaName)), 1, 0, '', 0, (SELECT TOP 1 UId FROM SysSchema WHERE name = @CardSchemaName), '', '', @ModuleCaption)
  
  */